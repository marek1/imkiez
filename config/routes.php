<?php

use Slim\Http\Response;
use Slim\Http\ServerRequest;
use Slim\App;

return function (App $app) {

   	$app->get('/', function (ServerRequest $request, Response $response) {
	   
	    $file = '../public/index.html';

	    $response->write(file_get_contents($file)); 

        return $response;
	    //app->render('myTemplate.php', array('id' => $id));
	});

    $app->post('/register', \App\Action\RegisterAction::class);

    $app->get('/token', \App\Action\TokenAction::class);

    $app->post('/login', \App\Action\UsersAction::class);

    $app->post('/image', \App\Action\ImageAction::class);

    $app->post('/video', \App\Action\VideoAction::class);

    $app->post('/route_file', \App\Action\RouteFileAction::class);

    $app->post('/gallery_file', \App\Action\GalleryFileAction::class);

    $app->post('/gallery_file_add_text', \App\Action\GalleryFileAddTextAction::class);

    $app->get('/thumb', \App\Action\ThumbAction::class);

    $app->get('/categories', \App\Action\CategoriesAction::class);

    $app->get('/cities', \App\Action\CitiesAction::class);

    $app->get('/place', \App\Action\PlaceGetAction::class);

    $app->get('/retrieve_assets', \App\Action\PlaceAssetsAction::class);

    $app->post('/place', \App\Action\PlacePostAction::class);

    $app->post('/update_place', \App\Action\PlaceUpdateAction::class);

    $app->post('/update_place_add_images', \App\Action\PlaceUpdateAddImagesAction::class);

    $app->post('/update_place_unset_images', \App\Action\PlaceUpdateUnsetImagesAction::class);

    $app->post('/update_place_add_videos', \App\Action\PlaceUpdateAddVideosAction::class);

    $app->post('/update_place_unset_videos', \App\Action\PlaceUpdateUnsetVideosAction::class);

    $app->post('/update_place_add_waypoints', \App\Action\PlaceUpdateAddWaypointsAction::class);

    $app->post('/places', \App\Action\PlacesAction::class);

    $app->post('/streets', \App\Action\StreetsAction::class);

    $app->get('/page', \App\Action\PageAction::class);

    $app->get('/homepage_view', \App\Action\HomepageViewAction::class);

    $app->get('/user', \App\Action\UserAction::class);

    $app->get('/user_profile', \App\Action\UserProfileGetAction::class);

    $app->post('/user_profile', \App\Action\UserProfilePostAction::class);

    $app->get('/user_stream', \App\Action\UserStreamAction::class);

    $app->get('/user_points', \App\Action\UserPointsGetAction::class);

    $app->post('/place_authorization', \App\Action\PlaceAuthorizationAction::class);

    $app->get('/place_queue', \App\Action\PlaceQueueAction::class);

    $app->get('/asset_queue', \App\Action\AssetQueueAction::class);

    $app->post('/asset_authorization', \App\Action\AssetAuthorizationAction::class);

    $app->post('/kiezbox', \App\Action\KiezboxAction::class);

    // $app->get('/migrate', \App\Action\MigrateAction::class);

    // Catch-all route to serve a 404 Not Found page if none of the routes match
    // NOTE: make sure this route is defined last
    $app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($req, $res) {
        $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
        return $handler($req, $res);
    });
};

?>