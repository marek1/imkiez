INSERT INTO `k128623_imkiez`.`categories` (
`category_id` ,
`name`
)
VALUES (
NULL , 'Bank / Bankfiliale'
), (
NULL , 'Geldautomat / ATM'
);

INSERT INTO `categories` (`category_id`, `name`, `description`) VALUES (NULL, 'Markt / Wochenmarkt', 'Essen, Getränke, Lokales'), (NULL, 'Flohmarkt', 'Gebrauchtes, Gebrauchte Sachen');
INSERT INTO `categories` (`category_id`, `name`, `description`) VALUES (NULL, 'Wissenschaftstheater', 'Planetarium');

ALTER TABLE `categories` ADD `description` TEXT NOT NULL ;
ALTER TABLE `categories` CHANGE `description` `description` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE `users` ADD `passwordhash` VARCHAR( 72 ) NOT NULL ;
