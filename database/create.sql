# create database k128623_imkiez

create table places(
	place_id int NOT NULL AUTO_INCREMENT,
	name varchar(255),
	description text,
	street_id int,
	house_number varchar(100),
	lat DOUBLE,
	lng DOUBLE,
	img_url varchar(255),
	thumb_url varchar(255),
	category_id int,
	tags text,
	ext_url varchar(255),
	int_url varchar(255),
	user_id int,
	google_maps_id varchar(255),
	created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (place_id),
	FOREIGN KEY (street_id) REFERENCES streets (street_id),
	FOREIGN KEY (category_id) REFERENCES categories (category_id),
	FOREIGN KEY (user_id) REFERENCES users (user_id)
)

create table streets(
    street_id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    lat DOUBLE,
    lng DOUBLE,
    zip varchar(255),
    city varchar(255),
    district varchar(255),
    county varchar(255),
    state varchar(255),
    countryName varchar(255),
    PRIMARY KEY (street_id)
)


create table categories(
	category_id int NOT NULL AUTO_INCREMENT,
	name varchar(255),
	PRIMARY KEY (category_id)
)

create table users(
    user_id int NOT NULL AUTO_INCREMENT,
    username varchar (255),
	PRIMARY KEY (user_id)
)

insert into users(username)
    VALUES
    ('mareks')


insert into categories(name)
	VALUES 
	('Restaurant'),
	('Café / Bäckerei'),
	('Kneipe / Bar'),
	('Biergarten'),
	('Arzt / Arztpraxis'),
	('Krankenhaus / Klinik'),
	('Supermarkt'),
	('Kiosk / Späti'),
	('Gallerie / Ausstellung'),
	('Firma / Unternehmen'),
	('Laden / Geschäft / Dienstleister'),
	('Indoor-Sport (inkl. Fitnessstudio)'),
	('Outdoor-Sport (inkl. Sportplatz)'),
	('Fotoautomat / Photobox'),
	('Baum'),
	('Park / Naherholungsgebiet'),
	('Teich'),
	('See'),
	('Sehenswürdigkeit / Gedenkstätte / Mahnmal'),
	('Station / Bahnhof'),
	('Toilette'),
	('Packstation'),
	('Postkasten'),
	('Ladesäule'),
	('Tankstelle'),
	('Parkplatz / Parkhaus / Parken'),
	('Club / Disco / Partylocation'),
	('Veranstaltungsort / Seminarräume'),
	('Therapie / Massage'),
	('Schnellrestaurant / Imbiss / Döner'),
	('Zoo / Tierpark'),
	('Kino'),
	('Kirche / Synagoge / Moschee'),
	('Hotel / Hostel / Ferienwohnung / Übernachtungsmöglichkeit'),
	('Schwimmbad / Badestelle'),
	('Flughafen / Airport'),
	('Hafen / Anlegestelle'),
	('Schule'),
	('Kindergarten'),
	('Amt'),
	('Container / Recycling / Müllverwertung'),
    ('Freizeiteinrichtung'),
    ('Spielplatz'),
	('Wald'),
	('Theater'),
	('Friseur / Frisör / Barbershop')


ALTER TABLE `places` ADD `active` TINYINT( 1 ) NOT NULL DEFAULT '1';

INSERT INTO `categories` (
`category_id` ,
`name`
)
VALUES (
NULL , 'Apotheke'
), (
NULL , 'Kaufhaus / Department Store '
);


create table sessions(
    session_id int NOT NULL AUTO_INCREMENT,
    token varchar(255),
    user_id int,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (session_id),
	FOREIGN KEY (user_id) REFERENCES users (user_id)
)


	/* OLD

	create table cities(
	city_id int NOT NULL AUTO_INCREMENT,
	name varchar(255),
	PRIMARY KEY (city_id)
    )

    insert into cities(name)
	VALUES
	('Berlin')

	 */


create table places_routes(
    route_id int NOT NULL AUTO_INCREMENT,
    place_id int,
    waypoints text,
    user_id int,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (route_id),
	FOREIGN KEY (place_id) REFERENCES places (place_id),
	FOREIGN KEY (user_id) REFERENCES users (user_id)
)

ALTER TABLE `places` ADD `route_id` INT DEFAULT NULL AFTER `active`;
ALTER TABLE places
ADD FOREIGN KEY (route_id) REFERENCES routes(route_id);

create table places_images(
    places_images_id int NOT NULL AUTO_INCREMENT,
    img_url varchar(255),
	thumb_url varchar(255),
    place_id int,
    user_id int,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    approved TIMESTAMP,
    approved_by_user_id int,
    PRIMARY KEY (places_images_id),
	FOREIGN KEY (place_id) REFERENCES places (place_id),
	FOREIGN KEY (user_id) REFERENCES users (user_id),
	FOREIGN KEY (approved_by_user_id) REFERENCES users (user_id)
)

ALTER TABLE `places` ADD `video_url` VARCHAR(255) NULL DEFAULT NULL AFTER `thumb_url`;

create table places_videos(
    places_videos_id int NOT NULL AUTO_INCREMENT,
    video_url varchar(255),
    place_id int,
    user_id int,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    approved TIMESTAMP,
    approved_by_user_id int,
    PRIMARY KEY (places_videos_id),
	FOREIGN KEY (place_id) REFERENCES places (place_id),
	FOREIGN KEY (user_id) REFERENCES users (user_id),
	FOREIGN KEY (approved_by_user_id) REFERENCES users (user_id)
)

create table pages (
    pages_id int NOT NULL AUTO_INCREMENT,
    title varchar(255),
    description TEXT,
    int_url varchar(255),
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (pages_id)
)

create table pages_places (
    pages_places_id int NOT NULL AUTO_INCREMENT,
    pages_id int,
    place_id int,
    PRIMARY KEY (pages_places_id),
	FOREIGN KEY (pages_id) REFERENCES pages (pages_id),
	FOREIGN KEY (place_id) REFERENCES places (place_id)
)

create table pages_views (
    pages_views_id int NOT NULL AUTO_INCREMENT,
    pages_id int,
    viewed TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (pages_views_id),
    FOREIGN KEY (pages_id) REFERENCES pages (pages_id)
)

create table places_views (
    places_views_id int NOT NULL AUTO_INCREMENT,
    place_id int,
    viewed TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (places_views_id),
	FOREIGN KEY (place_id) REFERENCES places (place_id)
)

create table places_authorizations (
    places_authorizations_id int NOT NULL AUTO_INCREMENT,
    place_id int,
    comment text,
    user_id int,
    approved TINYINT,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (places_authorizations_id),
	FOREIGN KEY (place_id) REFERENCES places (place_id),
	FOREIGN KEY (user_id) REFERENCES users (user_id)
)

create table assets_authorizations (
    assets_authorizations_id int NOT NULL AUTO_INCREMENT,
    places_images_id int,
    places_videos_id int,
    comment text,
    user_id int,
    approved TINYINT,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (assets_authorizations_id),
	FOREIGN KEY (places_images_id) REFERENCES places_images (places_images_id),
	FOREIGN KEY (places_videos_id) REFERENCES places_videos (places_videos_id),
	FOREIGN KEY (user_id) REFERENCES users (user_id)
)


create table users_points (
    users_points_id  int NOT NULL AUTO_INCREMENT,
    user_id int,
    points float,
    PRIMARY KEY (users_points_id),
	FOREIGN KEY (user_id) REFERENCES users (user_id)
)

create table homepage_views (
    homepage_views_id int NOT NULL AUTO_INCREMENT,
    user_agent varchar(255),
    language varchar(100),
    viewed TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (homepage_views_id)
)

create table searches (
    search_id int NOT NULL AUTO_INCREMENT,
    searchterm varchar(255),
    results int(10),
    searched TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (search_id)
)