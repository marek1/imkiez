<?php
namespace App\Domain\All\Service;

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

/**
 * Service.
 */
final class Email
{
    public $mail;
    public function __construct() {
        // Instantiation and passing `true` enables exceptions
        $this->mail = new PHPMailer(true);
    }

    public function getSender()
    {
        return 'info@imkiez.de';
    }

    public function getHtmlHeader()
    {
        $header = "MIME-Version: 1.0\r\n";
        $header .= "Content-type: text/html; charset=utf-8\r\n";
        $header .= "From: ".$this->getSender()."\r\n";
        $header .= "Reply-To: ".$this->getSender()."\r\n";
        return $header;
    }

    public function getHtmlTop()
    {
        return '<html><body><div style="width: 100%; max-width: 700px; margin: 0 10%;"><div><img src="https://imkiez.de/img/logo.jpg" style="max-width: 100px"></div>';
    }

    public function getHtmlBottom() {

        $footer = '<footer style="position: fixed; bottom: 0; padding: 10px; font-size: 10px; width: 100%; background-color: #f2f2f2;">© '.date("Y").' Marek Sonnabend | <a href="https://imkiez.de/" target="_blank">Webseite</a> | <a href="https://imkiez.de/impressum" target="_blank">Impressum</a> | <a href="https://imkiez.de/datenschutz" target="_blank">Datenschutz</a></footer>';

        return '</div>'.$footer.'</body></html>';

    }

    public function send($receiver, $subject, $nachricht) {

        echo "sending";

        try {
            //Server settings
            $this->mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $this->mail->CharSet = 'utf-8';
            $this->mail->setLanguage('de');
            $this->mail->isSMTP();                                            // Send using SMTP
            $this->mail->Host       = 'mx2e57.netcup.net';                    // Set the SMTP server to send through
            $this->mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $this->mail->Username   = 'info@imkiez.de';                     // SMTP username
            $this->mail->Password   = '_n5e8p1P';                               // SMTP password
            $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $this->mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            //Recipients
            $this->mail->setFrom('info@imkiez.de', 'ImKiez.de');
            $this->mail->addAddress($receiver, 'Hallo');     // Add a recipient
            $this->mail->addReplyTo('info@imkiez.de', 'ImKiez.de');

            // Attachments
            // $this->mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments

            // Content
            $this->mail->isHTML(true);                                  // Set email format to HTML
            $this->mail->Subject = $subject;
            $this->mail->Body    = $this->getHtmlTop() . $nachricht . $this->getHtmlBottom();
            $this->mail->AltBody = 'Leider kein HTML akiviert.';

            $this->mail->send();
        } catch (Exception $e) {
            error_log("Eine E-Mail an {$receiver} konnte NICHT gesendet werden. Mailer Error: {$this->mail->ErrorInfo}");
        }
    }

}