<?php

namespace App\Domain\All\Service;
use App\Domain\All\Repository\PagesRepository;
use UnexpectedValueException;

/**
 * Service.
 */
final class Pages
{
    private $repository;

    public function __construct(PagesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function retrieveByIntUrl(string $int_url)
    {
        return $this->repository->retrieveByIntUrl($int_url);
    }
}
