<?php

namespace App\Domain\All\Service;

use App\Domain\All\Repository\PlacesRepository;
use App\Domain\All\Repository\UsersRepository;
use UnexpectedValueException;

/**
 * Service.
 */
final class Places
{
    private $repository;

    public function __construct(PlacesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function retrieveBySearchterm($searchTerm, $lat, $lng, $user_id)
    {
        // Retrieve all
        return $this->repository->retrieveBySearchterm($searchTerm, $lat, $lng, $user_id);
    }

    public function migrateAssets()
    {
        return $this->repository->migrate();
    }
}