<?php

namespace App\Domain\All\Service;
use App\Domain\All\Repository\HomepageRepository;
use UnexpectedValueException;

/**
 * Service.
 */
final class Homepage
{
    private $repository;

    public function __construct(HomepageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function setHomepageView(string $language, string $user_agent)
    {
        return $this->repository->setHomepageView($language, $user_agent);
    }


}
