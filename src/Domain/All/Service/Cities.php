<?php

namespace App\Domain\All\Service;

use App\Domain\All\Repository\CitiesRepository;
use UnexpectedValueException;

/**
 * Service.
 */
final class Cities
{
    private $repository;

    public function __construct(CitiesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function retrieveAll()
    {
        // Retrieve all
        return $this->repository->retrieveAll();
    }
}