<?php

namespace App\Domain\All\Service;

use App\Domain\All\Data\StreetData;
use App\Domain\All\Repository\StreetsRepository;
use UnexpectedValueException;

/**
 * Service.
 */
final class Streets
{
    private $repository;

    public function __construct(StreetsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function save(StreetData $street)
    {

        // Retrieve all
        return $this->repository->save($street);
    }
}