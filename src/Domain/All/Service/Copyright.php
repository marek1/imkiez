<?php
namespace App\Domain\All\Service;

use PhpParser\Node\Scalar\String_;

/**
 * Service.
 */
final class Copyright
{
    public $images_as_preset;

    public function __construct()
    {
        // if it is one of the preset-places, then take original creator as user_id
        $this->images_as_preset = array(
            'public/uploads/atm.jpg',
            'public/uploads/amt.jpg',
            'public/uploads/arzt.jpg',
            'public/uploads/postkasten.jpg',
            'public/uploads/bolzplatz.jpg',
            'public/uploads/basketball.jpg',
            'public/uploads/tischtennis.jpg',
            'public/uploads/container.jpg',
            'public/uploads/packstation.jpg',
            'public/uploads/supermarket.jpg',
            'public/uploads/spaeti.jpg',
            'public/uploads/kiosk.jpg',
            'public/uploads/apotheke.jpg',
            'public/uploads/blumenladen.jpg',
            'public/uploads/fahrradladen.jpg',
            'public/uploads/friseur.jpg',
            'public/uploads/eis.jpg',
            'public/uploads/drogerie.jpg',
            'public/uploads/cafe.jpg',
            'public/uploads/wc.jpg',
            'public/uploads/waschsalon.jpg',
            'public/uploads/fotoautomat.jpg',
            'public/uploads/schnellrestaurant.jpg',
            'public/uploads/post.jpg',
            'public/uploads/kita.jpg'
        );
    }

    public function checkIfCopyrightIsOwned(string $img_url) {

        return in_array($img_url, $this->images_as_preset);

    }

}