<?php

namespace App\Domain\All\Service;

use App\Domain\All\Data\RegisterData;
use App\Domain\All\Data\UserPostData;
use App\Domain\All\Repository\UsersRepository;
use UnexpectedValueException;

/**
 * Service.
 */
final class Users
{
    private $repository;

    public function __construct(UsersRepository $repository)
    {
        $this->repository = $repository;
    }

    public function insertSession($userid)
    {
        // Retrieve all
        return $this->repository->insertSession($userid);
    }

    public function getSessionTime($token)
    {
        return $this->repository->getSessionTime($token);
    }

    public function save_points($user_id, $points)
    {
        return $this->repository->save_points($user_id, $points);
    }

    public function getUser($loginName)
    {
        return $this->repository->getUser($loginName);
    }

    public function getUsername($user_id)
    {
        return $this->repository->getUsername($user_id);
    }

    public function getUserEmail($user_id)
    {
        return $this->repository->getUserEmail($user_id);
    }

    public function getUserprofile($username)
    {
        return $this->repository->getUserprofile($username);
    }

    public function getStream($username)
    {
        return $this->repository->getStream($username);
    }

    public function getPoints($username)
    {
        return $this->repository->getPoints($username);
    }

    public function checkUsername($username, $token)
    {
        return $this->repository->checkUsername($username, $token);
    }

    public function updateUserprofile(UserPostData $user_data)
    {
        return $this->repository->updateUserprofile($user_data);
    }

    public function checkIfUserExists(RegisterData $registerData)
    {
        return $this->repository->checkIfUserExists($registerData);
    }

    public function createUser(RegisterData $registerData)
    {
        return $this->repository->createUser($registerData);
    }
}