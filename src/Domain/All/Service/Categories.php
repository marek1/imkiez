<?php

namespace App\Domain\All\Service;

use App\Domain\All\Repository\CategoriesRepository;
use UnexpectedValueException;

/**
 * Service.
 */
final class Categories
{
    private $repository;

    public function __construct(CategoriesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function retrieveAll()
    {
        // Retrieve all
        return $this->repository->retrieveAll();
    }
}