<?php

namespace App\Domain\All\Service;

use App\Action\AssetAuthorizationAction;
use App\Domain\All\Data\ImageAuthorizationData;
use App\Domain\All\Data\PlaceAuthorizationData;
use App\Domain\All\Data\PlaceData;
use App\Domain\All\Data\PlaceUpdateData;
use App\Domain\All\Data\PlacesImagesData;
use App\Domain\All\Data\PlacesVideosData;
use App\Domain\All\Data\PlacesWaypointsData;
use App\Domain\All\Data\VideoAuthorizationData;
use App\Domain\All\Repository\PlacesRepository;
use UnexpectedValueException;

/**
 * Service.
 */
final class Place
{
    private $repository;

    public function __construct(PlacesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function retrieveById(string $id)
    {
        return $this->repository->retrieveById($id);
    }

    public function retrieveByIdSlim(string $id)
    {
        return $this->repository->retrieveByIdSlim($id);
    }

    public function retrieveByIntUrl(string $int_url)
    {
        return $this->repository->retrieveByIntUrl($int_url);
    }

    public function save(PlaceData $place)
    {
        return $this->repository->save($place);
    }

    public function save_authorization(PlaceAuthorizationData $placeAuth)
    {
        return $this->repository->save_authorization($placeAuth);
    }

    public function set_approved($place_id)
    {
        return $this->repository->set_approved($place_id);
    }

    public function check_if_approved($place_id)
    {
        return $this->repository->check_if_approved($place_id);
    }

    public function get_place_creator($place_id)
    {
        return $this->repository->get_place_creator($place_id);
    }

    public function get_place_auths_diff(PlaceAuthorizationData $placeAuth)
    {
        return $this->repository->get_place_auths_diff($placeAuth);
    }

    public function update(PlaceUpdateData $place)
    {
        return $this->repository->update($place);
    }

    public function add_images(PlacesImagesData $place)
    {
        return $this->repository->add_images($place);
    }

    public function add_image_description($description, $img_url)
    {
        return $this->repository->add_image_description($description, $img_url);
    }

    public function add_video_description($description, $video_url)
    {
        return $this->repository->add_video_description($description, $video_url);
    }

    public function unset_images(PlacesImagesData $place)
    {
        return $this->repository->unset_images($place);
    }

    public function add_videos(PlacesVideosData $place)
    {
        return $this->repository->add_videos($place);
    }

    public function unset_videos(PlacesVideosData $place)
    {
        return $this->repository->unset_videos($place);
    }

    public function add_waypoints(PlacesWaypointsData $place)
    {
        return $this->repository->add_waypoints($place);
    }

    public function retrieve_non_approved($user_id)
    {
        return $this->repository->retrieve_non_approved($user_id);
    }

    public function retrieve_assets(string $id)
    {
        return $this->repository->retrieve_assets($id);
    }

    public function retrieve_non_approved_assets($user_id)
    {
        return $this->repository->retrieve_non_approved_assets($user_id);
    }

    public function check_if_image_approved($places_images_id)
    {
        return $this->repository->check_if_image_approved($places_images_id);
    }

    public function check_if_video_approved($places_videos_id)
    {
        return $this->repository->check_if_video_approved($places_videos_id);
    }

    public function get_images_creator($places_images_id)
    {
        return $this->repository->get_images_creator($places_images_id);
    }

    public function get_videos_creator($places_videos_id)
    {
        return $this->repository->get_videos_creator($places_videos_id);
    }

    public function save_images_authorization(ImageAuthorizationData $imageAuth)
    {
        return $this->repository->save_images_authorization($imageAuth);
    }

    public function save_videos_authorization(VideoAuthorizationData $videoAuth)
    {
        return $this->repository->save_videos_authorization($videoAuth);
    }

    public function set_image_approved($places_images_id)
    {
        return $this->repository->set_image_approved($places_images_id);
    }

    public function set_video_approved($places_videos_id)
    {
        return $this->repository->set_video_approved($places_videos_id);
    }
}