<?php

namespace App\Domain\All\Service;


/**
 * Service.
 */
final class Token
{

    private $users;

    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    public function getToken($allHeaders)
    {

        foreach ($allHeaders as $name => $value) {
            if ($name === 'X-Auth') {
                // echo "$name: $value\n";
                // 1aef36378a6a50477a54545f9b8a654f
                $token = $this->users->getSessionTime($value);
            }
        }

        return $token;

    }

    public function checkTokenExpiry($token)
    {

        $timeDiff = time() - strtotime($token['created']);

        $sessionLengthInHours = 24 * 7; // 24 * x Tage

        if ((int)$timeDiff / 3600 >= $sessionLengthInHours) {
            return false;
        }

        return true;
    }

}