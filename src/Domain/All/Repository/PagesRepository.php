<?php

namespace App\Domain\All\Repository;

use PDO;

/**
 * Repository.
 */
class PagesRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function setPageView($id) {

        $_id = [
            'id' => $id
        ];

        $sql = "INSERT INTO pages_views SET pages_id=:id;";

        $this->connection->prepare($sql)->execute($_id);

        return (int)$this->connection->lastInsertId();

    }

    public function retrieveByIntUrl($int_url)
    {

        $_int_url = [
            'int_url' => $int_url,
        ];

        // $this->increasePageView($_int_url);

        $sql = "SELECT 
                p.pages_id,
                p.title,
                p.description,
                (SELECT GROUP_CONCAT(NULLIF(pp.place_id, '')) from pages_places as pp where pp.pages_id = p.pages_id) as places
                from pages as p 
                where p.int_url=:int_url
                and p.active = 1";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_int_url);

        $returnArray = $prep->fetch();

        $this->setPageView($returnArray['pages_id']);

        return $returnArray;

    }

}
?>