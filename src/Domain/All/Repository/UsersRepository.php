<?php

namespace App\Domain\All\Repository;

use App\Domain\All\Data\RegisterData;
use App\Domain\All\Data\UserPostData;
use PDO;

/**
 * Repository.
 */
class UsersRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }


    public function insertSession($userId)
    {

        $token = md5($userId . '+#*' . time());

        $row = [
            'token' => $token,
            'user_id' => $userId
        ];

        $sql = "INSERT INTO sessions SET 
                token=:token, 
                user_id=:user_id;";

        $this->connection->prepare($sql)->execute($row);

        return $token;

    }

    public function getSessionTime($token)
    {

        $_token = [
            'token' => $token
        ];

        $sql = "SELECT * from sessions where token=:token;";


        $prep = $this->connection->prepare($sql);

        $prep->execute($_token);

        return $prep->fetch();

    }

    public function save_points($user_id, $points)
    {
        $row = [
            'user_id' => $user_id,
            'points' => $points
        ];
        
        $sql = "INSERT INTO users_points 
                SET user_id=:user_id, 
                points=:points;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }

    public function getUsername($user_id) {

        $row = [
            'user_id' => $user_id
        ];

        $sql = "SELECT
                username
                FROM users
                WHERE user_id=:user_id
                AND active = 1;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->fetch();

    }

    public function getUserEmail($user_id) {

        $row = [
            'user_id' => $user_id
        ];

        $sql = "SELECT
                login as email
                FROM users
                WHERE user_id=:user_id
                AND active = 1;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->fetch();

    }

    public function getUser($loginName)
    {

        $_login = [
            'login' => $loginName
        ];

        $sql = "SELECT * from users where login=:login AND active = 1";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_login);

        return $prep->fetch();

    }


    public function getUserprofile($username)
    {

        $_username = [
            'username' => $username
        ];

        $sql = "SELECT 
            u.username,
            u.description,
            u.img_url,
            u.thumb_url
            from users as u
            where u.username=:username
            AND u.active = 1";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_username);

        return $prep->fetch();

    }

    public function getPoints($username)
    {

        $_username = [
            'username' => $username
        ];

        $sql = "SELECT 
        sum(up.points) as points 
        from users_points as up
        JOIN users as u where u.user_id = up.user_id AND u.username=:username AND u.active = 1";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_username);

        return $prep->fetch();

    }

    public function getStream($username)
    {

        $_username = [
            'username' => $username
        ];

        $resultArray = array();

        $sql = "SELECT 
        pi.img_url,
        pi.thumb_url,
        p.name,
        p.int_url,
        pi.created
        FROM users as u
        JOIN places_images as pi on u.user_id = pi.user_id and pi.active = 1
        JOIN places as p on p.place_id = pi.place_id AND p.active = 1
        WHERE u.username=:username
        AND u.active = 1
        ORDER BY pi.created DESC
        LIMIT 20";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_username);

        $result = $prep->fetchAll();

        foreach ($result as $res) {
            array_push($resultArray, $res);
        }

        $sql1 = "SELECT 
        pv.video_url,
        p.name,
        p.int_url,
        pv.created
        FROM users as u
        JOIN places_videos as pv on u.user_id = pv.user_id and pv.active = 1 
        JOIN places as p on p.place_id = pv.place_id AND p.active = 1
        WHERE u.username=:username
        AND u.active = 1
        ORDER BY pv.created DESC
        LIMIT 20";

        $prep1 = $this->connection->prepare($sql1);

        $prep1->execute($_username);

        $result1 = $prep1->fetchAll();

        foreach ($result1 as $res) {
            array_push($resultArray, $res);
        }

        // sort by created
        usort($resultArray, function($a, $b) {
            return $b['created'] <=> $a['created'];
        });

        return $resultArray;

    }

    public function checkUsername($username, $token)
    {

        $_data = [
            'username' => $username,
            'token' => $token
        ];

        $sql1 = "SELECT
            *
            from users as u 
            JOIN sessions as s ON s.user_id = u.user_id and s.token=:token
            where u.username=:username
            AND u.active = 1";

        $prep1 = $this->connection->prepare($sql1);

        $prep1->execute($_data);

        $prep1->fetch();

        return $prep1->rowCount();

    }

    public function updateUserprofile(UserPostData $user_data)
    {

        $_user_data = [
            'username' => $user_data->username,
            'description' => $user_data->description,
            'img_url' => $user_data->img_url,
            'thumb_url' => $user_data->thumb_url
        ];

        $sql = "UPDATE users SET
                description=:description,
                img_url=:img_url,
                thumb_url=:thumb_url
                WHERE username=:username";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_user_data);

        return $prep->rowCount();

    }

    public function checkIfUserExists(RegisterData $registerData)
    {

        $_user = [
            'login' => $registerData->email,
            'username' => $registerData->username
        ];

        $sql = "SELECT * from users where (login=:login or username=:username) AND active = 1";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_user);

        return $prep->fetch();

    }

    public function createUser(RegisterData $registerData)
    {
        $_user = [
            'login' => $registerData->email,
            'username' => $registerData->username,
            'passwordhash' => password_hash($registerData->password, PASSWORD_DEFAULT)
        ];

        $sql = "INSERT INTO users 
                SET login=:login, 
                username=:username,
                passwordhash=:passwordhash;";

        $this->connection->prepare($sql)->execute($_user);

        return (int)$this->connection->lastInsertId();

    }

}

?>