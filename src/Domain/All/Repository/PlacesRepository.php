<?php

namespace App\Domain\All\Repository;

use App\Domain\All\Data\ImageAuthorizationData;
use App\Domain\All\Data\PlaceAuthorizationData;
use App\Domain\All\Data\PlaceData;
use App\Domain\All\Data\PlacesImagesData;
use App\Domain\All\Data\PlacesVideosData;
use App\Domain\All\Data\PlacesWaypointsData;
use App\Domain\All\Data\PlaceUpdateData;
use App\Domain\All\Data\VideoAuthorizationData;
use App\Domain\All\Service\Copyright;
use PDO;
use function DI\string;

/**
 * Repository.
 */
class PlacesRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;
    private $copyright;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection, Copyright $copyright)
    {
        $this->connection = $connection;
        $this->copyright = $copyright;
    }

    public function migrate()
    {
        // iterate through each place


        $sql = 'SELECT * from places';

        $results = $this->connection->query($sql)->fetchAll();

        foreach ($results as $row) {

            //echo "...".$row->img_url." ... ";

            $_row = [
                'img_url' => $row['img_url'],
                'thumb_url' => $row['thumb_url'],
                'place_id' => $row['place_id'],
                'user_id' => 1
            ];

            $_sql = "INSERT places_images SET
                img_url=:img_url,
                thumb_url=:thumb_url,
                place_id=:place_id,
                user_id=:user_id";

            $this->connection->prepare($_sql)->execute($_row);

        }

        return true;

    }

    public function setPlaceView($id) {

        $_id = [
            'id' => $id
        ];

        $sql = "INSERT INTO places_views SET place_id=:id;";

        $this->connection->prepare($sql)->execute($_id);

        return (int)$this->connection->lastInsertId();

    }

    public function retrieveByIdSlim($id) {

        $_id = [
            'id' => $id,
        ];

        // return $this->checkIfUrlExists($int_url);
        // get all PLAYING GROUNDS
        $sql = "SELECT
            p.name,
            p.int_url
            FROM places as p
            WHERE p.active = 1
            and p.place_id=:id";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_id);

        return $prep->fetch();
    }

    public function retrieveById($id) {

        $_id = [
            'id' => $id,
        ];

        // return $this->checkIfUrlExists($int_url);
        // get all PLAYING GROUNDS
        $sql = "SELECT
            p.place_id,
            p.name,
            p.description,
            p.house_number,
            p.lat,
            p.lng,
            p.tags,
            p.category_id,
            c.name as category,
            s.name as street,
            s.zip as zip,
            s.city as city,
            p.ext_url,
            p.int_url,
            p.approved,
            (SELECT GROUP_CONCAT(NULLIF(pi.thumb_url, '')) from places_images as pi where p.place_id = pi.place_id and pi.active = 1) as thumbs,
            (SELECT GROUP_CONCAT(NULLIF(pi.img_url, '')) from places_images as pi where p.place_id = pi.place_id and pi.active = 1) as images,
            (SELECT GROUP_CONCAT(NULLIF(pv.video_url, '')) from places_videos as pv where p.place_id = pv.place_id and pv.active = 1) as videos,
            (SELECT waypoints from places_routes as r where r.place_id = p.place_id and r.active = 1 ORDER BY created DESC LIMIT 1) as waypoints
            from places as p
            LEFT JOIN streets as s ON s.street_id = p.street_id
            LEFT JOIN categories as c ON c.category_id = p.category_id
            WHERE p.active = 1
            and p.place_id = :id";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_id);

        $result = $prep->fetch();

        if (!$result) {
            return $result;
        }
        //echo "thumbs : " . $result['thumbs'];
        if (strlen($result['thumbs']) > 0 && strlen($result['images']) > 0) {
            $result['thumbs'] = explode(",", $result['thumbs']);
            $result['images'] = explode(",", $result['images']);
        } else {
            $result['thumbs'] = [];
            $result['images'] = [];
        }
        if (strlen($result['videos']) > 0 && strlen($result['videos']) > 0) {
            $result['videos'] = explode(",", $result['videos']);
        } else {
            $result['videos'] = [];
        }
        if (strlen($result['waypoints']) > 0) {
            $result['waypoints'] = json_decode($result['waypoints']);
        } else {
            $result['waypoints'] = [];
        }

        return $result;
    }

    public function retrieve_assets(string $id) {

        $_id = [
            'id' => $id,
        ];

        $resultArray = array();

        // example: 11332

        $sql = "SELECT
        pi.img_url,
        pi.thumb_url,
        pi.description,
        u.username
        FROM places_images as pi
        JOIN users as u on u.user_id = pi.user_id
        WHERE pi.place_id=:id
        AND pi.active = 1
        AND pi.approved = 1";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_id);

        $result = $prep->fetchAll();

        foreach ($result as $res) {
            array_push($resultArray, $res);
        }

        $sql1 = "SELECT
        pv.video_url,
        pv.description,
        u.username
        FROM places_videos as pv
        JOIN users as u on u.user_id = pv.user_id
        WHERE pv.place_id=:id
        AND pv.active = 1
        AND pv.approved = 1";

        $prep1 = $this->connection->prepare($sql1);

        $prep1->execute($_id);

        $result1 = $prep1->fetchAll();

        foreach ($result1 as $res) {
            array_push($resultArray, $res);
        }

        $sql2 = "SELECT
        pr.waypoints,
        u.username
        FROM places_routes as pr
        JOIN users as u on u.user_id = pr.user_id
        WHERE pr.place_id=:id
        AND pr.active = 1
        ORDER BY created DESC
        LIMIT 1
        ";


        $prep2 = $this->connection->prepare($sql2);

        $prep2->execute($_id);

        $result2 = $prep2->fetchAll();

        foreach ($result2 as $res) {
            if (strlen($res['waypoints']) > 0) {
                $res['waypoints'] = json_decode($res['waypoints']);
            }
            array_push($resultArray, $res);
        }

        return $resultArray;
    }

    public function retrieveByIntUrl($int_url) {

        $_int_url = [
            'int_url' => $int_url,
        ];

        // return $this->checkIfUrlExists($int_url);
        $sql = "SELECT
            p.place_id,
            p.name,
            p.description,
            p.house_number,
            p.lat,
            p.lng,
            p.tags,
            p.category_id,
            c.name as category,
            s.name as street,
            s.zip as zip,
            s.city as city,
            p.ext_url,
            p.int_url,
            p.approved,
            u.username
            from places as p
            LEFT JOIN streets as s ON s.street_id = p.street_id
            LEFT JOIN categories as c ON c.category_id = p.category_id
            LEFT JOIN users as u ON u.user_id = p.user_id
            WHERE p.active = 1
            and p.int_url = :int_url";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_int_url);

        $result = $prep->fetch();

        $this->setPlaceView($result['place_id']);

        if (!$result) {
            return $result;
        }

        return $result;
    }

    public function getNameOfCity($id)
    {

        $_id = [
            'id' => $id,
        ];

        $sql = "SELECT * from streets where street_id=:id";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_id);

        return $prep->fetch();

    }

    public function checkIfUrlExists($int_url)
    {

        $_int_url = [
            'inturl' => $int_url,
        ];

        $sql = "SELECT * from places where int_url=:inturl";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_int_url);

        return $prep->fetch();

    }

    public function replaceSonderzeichen($string)
    {
        $string = str_replace("ä", "ae", $string);
        $string = str_replace("ü", "ue", $string);
        $string = str_replace("ö", "oe", $string);
        $string = str_replace("Ä", "Ae", $string);
        $string = str_replace("Ü", "Ue", $string);
        $string = str_replace("Ö", "Oe", $string);
        $string = str_replace("ß", "ss", $string);
        $string = str_replace("é", "e", $string);
        $string = str_replace(",", "-", $string);
        $string = str_replace(".", "-", $string);
        $string = str_replace("´", "", $string);
        $string = str_replace("(", "", $string);
        $string = str_replace(")", "", $string);
        $string = str_replace("&", "und", $string);
        $string = str_replace("/", "-", $string);
        $string = str_replace("*", "-", $string);
        $string = str_replace(";", "-", $string);
        $string = str_replace("%", "-", $string);
        $string = str_replace("'", "-", $string);
        return $string;
    }

    public function save(PlaceData $place)
    {
        $foundCity = $this->getNameOfCity((int)$place->street_id);

        $intUrl = strtolower(preg_replace("/[^\w]+/", "-", urlencode($this->replaceSonderzeichen($place->name))));

        $intUrl .= '-' . strtolower(preg_replace("/[^\w]+/", "-", urlencode($this->replaceSonderzeichen($foundCity['name']))));

        if ($foundCity['district']) {
            $intUrl .= '-' . strtolower(preg_replace("/[^\w]+/", "-", urlencode($this->replaceSonderzeichen($foundCity['district']))));
        }

        if ($foundCity['city']) {
            $intUrl .= '-' . strtolower(preg_replace("/[^\w]+/", "-", urlencode($this->replaceSonderzeichen($foundCity['city']))));
        }

        if ($foundCity['county'] && $foundCity['city'] != $foundCity['county']) {
            $intUrl .= '-' . strtolower(preg_replace("/[^\w]+/", "-", urlencode($this->replaceSonderzeichen($foundCity['county']))));
        }

        // test if this is already taken
        if ($this->checkIfUrlExists($intUrl)) {
            // if so , append timestamp
            $intUrl = $intUrl . '-' . time();
        }

        $row = [
            'name' => $place->name,
            'description' => $place->description,
            'street_id' => $place->street_id,
            'house_number' => $place->house_number,
            'lat' => $place->lat,
            'lng' => $place->lng,
            'category_id' => $place->category_id,
            'tags' => $place->tags,
            'ext_url' => $place->ext_url,
            'int_url' => $intUrl,
            'user_id' => $place->user_id
        ];

        //echo "this could be the int url " . preg_replace("/[^\w]+/", "-", $place->int_url);

        $sql = "INSERT INTO places SET
                name=:name,
                description=:description,
                street_id=:street_id,
                house_number=:house_number,
                lat=:lat,
                lng=:lng,
                category_id=:category_id,
                tags=:tags,
                ext_url=:ext_url,
                int_url=:int_url,
                user_id=:user_id;";

        $this->connection->prepare($sql)->execute($row);

        $lastInsertId = (int)$this->connection->lastInsertId();

        if (!$lastInsertId) {
            return null;
        }

        if ($place->img_url && $place->thumb_url) {
            // check if this is a precat image
            if ($this->copyright->checkIfCopyrightIsOwned($place->img_url)) {
                $row1 = [
                    'img_url' => $place->img_url,
                    'thumb_url' => $place->thumb_url,
                    'lastInsertId' => $lastInsertId,
                    'user_id' => 1
                ];

                $sql1 = "INSERT INTO places_images SET
                img_url=:img_url,
                thumb_url=:thumb_url,
                place_id=:lastInsertId,
                user_id=:user_id,
                approved = 1;";

            } else {
                $row1 = [
                    'img_url' => $place->img_url,
                    'thumb_url' => $place->thumb_url,
                    'lastInsertId' => $lastInsertId,
                    'user_id' => $place->user_id
                ];

                $sql1 = "INSERT INTO places_images SET
                img_url=:img_url,
                thumb_url=:thumb_url,
                place_id=:lastInsertId,
                user_id=:user_id;";
            }



            $this->connection->prepare($sql1)->execute($row1);
        }

        if ($place->video_url) {
            $row2 = [
                'video_url' => $place->video_url,
                'lastInsertId' => $lastInsertId,
                'user_id' => $place->user_id
            ];

            $sql2 = "INSERT INTO places_videos SET
                video_url=:video_url,
                place_id=:lastInsertId,
                user_id=:user_id;";

            $this->connection->prepare($sql2)->execute($row2);
        }

        return $intUrl;

    }

    public function add_points($user_id, $points)
    {
        $row = [
            'user_id' => $user_id,
            'points' => $points
        ];

        $sql = "INSERT INTO users_points SET
                user_id=:user_id,
                points=:points;";

        $this->connection->prepare($sql)->execute($row);

        $lastInsertId = (int)$this->connection->lastInsertId();

        return $lastInsertId;
    }

    public function get_place_auths_diff(PlaceAuthorizationData $placeAuth)
    {
        $row = [
            'place_id' => $placeAuth->place_id
        ];

        $sql1 = "SELECT
        *
        from places_authorizations
        WHERE place_id=:place_id
        AND approved=1";

        $prep1 = $this->connection->prepare($sql1);

        $prep1->execute($row);

        $approved = $prep1->rowCount();

        $sql2 = "SELECT
        *
        from places_authorizations
        WHERE place_id=:place_id
        AND approved=0";

        $prep2 = $this->connection->prepare($sql2);

        $prep2->execute($row);

        $declined = $prep2->rowCount();

        return $approved - $declined;

    }

    public function set_approved($place_id)
    {
        $row = [
            'place_id' => $place_id
        ];

        $sql = "UPDATE places SET
                approved=1
                WHERE place_id=:place_id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->rowCount();

    }

    public function set_image_approved($places_images_id)
    {
        $row = [
            'places_images_id' => $places_images_id
        ];

        $sql = "UPDATE places_images SET
                approved=1
                WHERE places_images_id=:places_images_id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->rowCount();
    }

    public function set_video_approved($places_videos_id)
    {
        $row = [
            'places_videos_id' => $places_videos_id
        ];

        $sql = "UPDATE places_videos SET
                approved=1
                WHERE places_videos_id=:places_videos_id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->rowCount();
    }

    public function check_if_image_approved($places_images_id) {

        $row = [
            'places_images_id' => $places_images_id
        ];

        $sql = "SELECT
                approved
                FROM places_images
                WHERE places_images_id=:places_images_id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->fetch();
    }

    public function check_if_video_approved($places_videos_id) {

        $row = [
            'places_videos_id' => $places_videos_id
        ];

        $sql = "SELECT
                approved
                FROM places_videos
                WHERE places_videos_id=:places_videos_id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->fetch();
    }

    public function check_if_approved($place_id)
    {
        $row = [
            'place_id' => $place_id
        ];

        $sql = "SELECT
                approved
                FROM places
                WHERE place_id=:place_id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->fetch();
    }

    public function get_images_creator($places_images_id)
    {
        $row = [
            'places_images_id' => $places_images_id
        ];

        $sql = "SELECT
                user_id
                FROM places_images
                WHERE places_images_id=:places_images_id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->fetch();

    }

    public function get_videos_creator($places_videos_id)
    {
        $row = [
            'places_videos_id' => $places_videos_id
        ];

        $sql = "SELECT
                user_id
                FROM places_videos
                WHERE places_videos_id=:places_videos_id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->fetch();

    }


    public function get_place_creator($place_id)
    {
        $row = [
            'place_id' => $place_id
        ];

        $sql = "SELECT
                user_id
                FROM places
                WHERE place_id=:place_id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->fetch();

    }

    public function save_images_authorization(ImageAuthorizationData $imageAuth)
    {

        $row = [
            'places_images_id' => $imageAuth->places_images_id,
            'user_id' => $imageAuth->user_id,
        ];

        $sql = "SELECT * FROM assets_authorizations WHERE
                places_images_id=:places_images_id AND user_id=:user_id";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        $noOfAuths = $prep->rowCount();

        if ($noOfAuths < 1) {
            $row_1 = [
                'places_images_id' => $imageAuth->places_images_id,
                'comment' => $imageAuth->comment,
                'user_id' => $imageAuth->user_id,
                'approved' => $imageAuth->approved
            ];

            $sql_1 = "INSERT INTO assets_authorizations SET
                places_images_id=:places_images_id,
                comment=:comment,
                user_id=:user_id,
                approved=:approved;";

            $this->connection->prepare($sql_1)->execute($row_1);

            $lastInsertId = (int)$this->connection->lastInsertId();

            return $lastInsertId;
        }

        return 0;
    }


    public function save_videos_authorization(VideoAuthorizationData $videoAuth)
    {

        $row = [
            'places_videos_id' => $videoAuth->places_videos_id,
            'user_id' => $videoAuth->user_id,
        ];

        $sql = "SELECT * FROM assets_authorizations WHERE
                places_videos_id=:places_videos_id AND user_id=:user_id";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        $noOfAuths = $prep->rowCount();

        if ($noOfAuths < 1) {
            $row_1 = [
                'places_videos_id' => $videoAuth->places_videos_id,
                'comment' => $videoAuth->comment,
                'user_id' => $videoAuth->user_id,
                'approved' => $videoAuth->approved
            ];

            $sql_1 = "INSERT INTO assets_authorizations SET
                places_videos_id=:places_videos_id,
                comment=:comment,
                user_id=:user_id,
                approved=:approved;";

            $this->connection->prepare($sql_1)->execute($row_1);

            $lastInsertId = (int)$this->connection->lastInsertId();

            return $lastInsertId;
        }

        return 0;
    }

    public function save_authorization(PlaceAuthorizationData $placeAuth)
    {
        $row = [
            'place_id' => $placeAuth->place_id,
            'user_id' => $placeAuth->user_id,
        ];

        $sql = "SELECT * FROM places_authorizations WHERE
                place_id=:place_id AND user_id=:user_id";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        $noOfAuths = $prep->rowCount();

        if ($noOfAuths < 1) {

            $row_1 = [
                'place_id' => $placeAuth->place_id,
                'comment' => $placeAuth->comment,
                'user_id' => $placeAuth->user_id,
                'approved' => $placeAuth->approved
            ];

            $sql_1 = "INSERT INTO places_authorizations SET
                place_id=:place_id,
                comment=:comment,
                user_id=:user_id,
                approved=:approved;";

            $this->connection->prepare($sql_1)->execute($row_1);

            $lastInsertId = (int)$this->connection->lastInsertId();

            return $lastInsertId;
        }

        return 0;

    }

    public function update(PlaceUpdateData $place)
    {
        $row = [
            'place_id' => $place->place_id,
            'description' => $place->description,
            'house_number' => $place->house_number,
            'tags' => $place->tags,
            'ext_url' => $place->ext_url,
            'category_id' => $place->category_id
        ];

        $sql = "UPDATE places SET
                description=:description,
                house_number=:house_number,
                tags=:tags,
                ext_url=:ext_url,
                category_id=:category_id
                WHERE place_id=:place_id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->rowCount();

    }

    function add_image_description($description, $img_url)
    {
        $row = [
            'description' => $description,
            'img_url' => $img_url
        ];

        $sql = "UPDATE places_images SET
                description=:description
                WHERE
                img_url=:img_url;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }

    function add_images(PlacesImagesData $place)
    {
        $row = [
            'place_id' => $place->place_id,
            'img_url' => $place->img_url,
            'thumb_url' => $place->thumb_url,
            'user_id' => $place->user_id
        ];

        $sql = "INSERT INTO places_images SET
                place_id=:place_id,
                img_url=:img_url,
                thumb_url=:thumb_url,
                user_id=:user_id;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }

    function unset_images(PlacesImagesData $place)
    {
        $row = [
            'place_id' => $place->place_id,
            'img_url' => $place->img_url,
            'thumb_url' => $place->thumb_url,
            'user_id' => $place->user_id
        ];

        $sql = "UPDATE places_images
                SET active = 0
                WHERE place_id=:place_id
                AND img_url=:img_url
                AND thumb_url=:thumb_url
                AND user_id=:user_id;";


        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->rowCount();

    }

    function add_video_description($description, $video_url)
    {
        $row = [
            'description' => $description,
            'video_url' => $video_url
        ];

        $sql = "UPDATE places_videos SET
                description=:description
                WHERE video_url=:video_url
                ORDER BY created DESC
                LIMIT 1;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }

    function add_videos(PlacesVideosData $place)
    {
        $row = [
            'place_id' => $place->place_id,
            'video_url' => $place->video_url,
            'user_id' => $place->user_id
        ];

        $sql = "INSERT INTO places_videos SET
                place_id=:place_id,
                video_url=:video_url,
                user_id=:user_id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->rowCount();
    }

    function unset_videos(PlacesVideosData $place)
    {
        $row = [
            'place_id' => $place->place_id,
            'video_url' => $place->video_url,
            'user_id' => $place->user_id
        ];

        $sql = "UPDATE places_videos
                SET active = 0
                WHERE place_id=:place_id
                AND video_url=:video_url
                AND user_id=:user_id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        return $prep->rowCount();

    }

    function add_waypoints(PlacesWaypointsData $place)
    {
        $row = [
            'place_id' => $place->place_id,
            'waypoints' => $place->waypoints,
            'user_id' => $place->user_id
        ];

        $sql = "INSERT INTO places_routes SET
                place_id=:place_id,
                waypoints=:waypoints,
                user_id=:user_id;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }

    function retrieve_non_approved($user_id)
    {

        $row = [
            'user_id' => $user_id
        ];

        $sql = "SELECT
            p.*,
            c.name as category,
            (SELECT COUNT(*) FROM places_authorizations as pa WHERE pa.place_id = p.place_id and pa.user_id=:user_id) as already_approved
            FROM places as p
            LEFT JOIN categories as c ON c.category_id = p.category_id
            WHERE p.approved = 0
            AND p.active = 1
            AND p.user_id!=:user_id";

        $prep = $this->connection->prepare($sql);

        $prep->execute($row);

        $results = $prep->fetchAll();

        $resultArray = array();
        foreach ($results as $res) {
            if ($res['already_approved'] < 1) {
                array_push($resultArray, $res);
            }
        }

        return $resultArray;

    }

    function retrieve_non_approved_assets($user_id)
    {

        $_id = [
            'user_id' => $user_id
        ];

        $resultArray = array();

        $sql = "SELECT
        p.name,
        p.int_url,
        pi.places_images_id,
        pi.img_url,
        pi.thumb_url,
        pi.description,
        pi.approved,
        (SELECT COUNT(*) FROM assets_authorizations as aa WHERE aa.places_images_id = pi.places_images_id and aa.user_id=:user_id) as already_approved
        FROM places_images as pi
        JOIN places as p on p.place_id = pi.place_id
        WHERE pi.active = 1
        AND pi.approved = 0
        AND pi.user_id!=:user_id";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_id);

        $result = $prep->fetchAll();

        foreach ($result as $res) {
            if ($res['already_approved'] < 1) {
                array_push($resultArray, $res);
            }
        }

        $sql1 = "SELECT
        p.name,
        p.int_url,
        pv.places_videos_id,
        pv.video_url,
        pv.description,
        pv.approved,
        (SELECT COUNT(*) FROM assets_authorizations as aa WHERE aa.places_videos_id = pv.places_videos_id and aa.user_id=:user_id) as already_approved
        FROM places_videos as pv
        JOIN places as p on p.place_id = pv.place_id
        WHERE pv.active = 1
        AND pv.approved = 0
        AND pv.user_id!=:user_id";

        $prep1 = $this->connection->prepare($sql1);

        $prep1->execute($_id);

        $result1 = $prep1->fetchAll();

        foreach ($result1 as $res) {
            if ($res['already_approved'] < 1) {
                array_push($resultArray, $res);
            }
        }

        return $resultArray;
    }

    private function get_distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

    public function saveSearch($searchTerm, $results, $user_id) {

        $_data = [
            'searchTerm' => $searchTerm,
            'user_id' => $user_id,
            'results' => (int)$results
        ];

        $sql = "INSERT INTO searches
                SET
                searchterm=:searchTerm,
                user_id=:user_id,
                results=:results;";

        $this->connection->prepare($sql)->execute($_data);

        return (int)$this->connection->lastInsertId();

    }

    public function retrieveBySearchterm($searchTerm, $lat, $lng, $user_id)
    {

        // break searchterms

        $searchTerms = explode(" ", strtolower($searchTerm));

        // $lowerSearchTerm = strtolower($searchTerm);

        $implodedSearchTerms = implode("','", $searchTerms);

        // var_dump($implodedSearchTerms);

        // if (sizeof($searchTerms) > 1) {
        // get all places

        $resultSet = array();
        $final_resultSet = array();

        foreach ($searchTerms as $term) {

            $_term = '%' . $term . '%';

            $sql2 = "SELECT
            p.place_id,
            p.name,
            p.description,
            p.house_number,
            p.lat,
            p.lng,
            p.tags,
            p.category_id,
            c.name as category,
            s.name as street,
            s.zip as zip,
            s.city as city,
            p.ext_url,
            p.int_url,
            p.approved,
            u.username
            from places as p
            LEFT JOIN streets as s ON s.street_id = p.street_id
            LEFT JOIN categories as c ON c.category_id = p.category_id
            LEFT JOIN users as u ON u.user_id = p.user_id
            WHERE (
            LOWER(p.name) LIKE :term OR
            LOWER(p.description) LIKE :term OR
            LOWER(p.tags) LIKE :term OR
            LOWER(s.name) LIKE :term OR
            LOWER(s.city) LIKE :term OR
            LOWER(s.district) LIKE :term OR
            LOWER(s.county) LIKE :term OR
            LOWER(c.name) LIKE :term OR
            LOWER(c.description) LIKE :term
            ) AND p.active = 1";

            $prep2 = $this->connection->prepare($sql2);
            $prep2->bindParam(':term', $_term);
            $prep2->execute();
            $arr2 = $prep2->fetchAll();
            $resultSet = array_merge($resultSet, $arr2);
            $max_distance = 1; // KM!

            // now go through the result set and get only those within
            // a distance of
            // $center_lat = 52.522509;
            // $center_lng = 13.413088;
            // $max_distance_when_no_coordinates = 15;

            // go trough resultset
            foreach ($resultSet as $result) {
                // if there is a lat+lng (which is not the set center),
                // get only results within $max_distance km
                if ($lat != 0 && $lng != 0 && strval($lat) != '52.5364551' && strval($lng) != '13.4105201') {
                    // could set zoom level : : : $final_resultSet[0] = 16;
                    // $distance = $this->get_distance($lat, $lng, $result['lat'], $result['lng'], "K");

                    $result['distance'] = $this->get_distance($lat, $lng, $result['lat'], $result['lng'], "K");
                    //NOT CHECKING DISTANCE at the moment:
                    //if ($this->get_distance($lat, $lng, $result['lat'], $result['lng'], "K") <= $max_distance) {
                        array_push($final_resultSet, $result);
                    //}
                } else {
                    // could set zoom level : : : $final_resultSet[0] = 16;
                    $result['distance'] = 0;
                    array_push($final_resultSet, $result);
                }

            }

            // if nothing nearby was found, take the resultset (to show something at least)
            if (sizeof($final_resultSet) == 0 && sizeof($resultSet) > 0) {
                $final_resultSet = $resultSet;
            }

        }

        $this->saveSearch($searchTerm, sizeof($final_resultSet), $user_id);

        return $final_resultSet;

    }
}

?>
