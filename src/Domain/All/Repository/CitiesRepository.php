<?php

namespace App\Domain\All\Repository;

use PDO;

/**
 * Repository.
 */
class CitiesRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }


    public function retrieveAll()
    {

        $sql = "SELECT * from cities";

        $prep = $this->connection->prepare($sql);

        $prep->execute();

        return $prep->fetchAll();

    }
}

?>