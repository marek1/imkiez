<?php

namespace App\Domain\All\Repository;

use App\Domain\All\Data\StreetData;
use PDO;

/**
 * Repository.
 */
class StreetsRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function save(StreetData $street)
    {

        $foundEntry = $this->findExistingEntry($street->name);

        if (sizeof($foundEntry) > 0) {
            return $foundEntry[0];
        }

        $row = [
            'name' => $street->name,
            'lat' => $street->lat,
            'lng' => $street->lng,
            'zip' => $street->zip,
            'city' => $street->city,
            'district' => $street->district,
            'county' => $street->county,
            'state' => $street->state,
            'countryName' => $street->countryName
        ];

        $sql = "INSERT INTO streets SET 
                name=:name, 
                lat=:lat,
                lng=:lng,
                zip=:zip,
                city=:city,
                district=:district,
                county=:county,
                state=:state,
                countryName=:countryName";

        $this->connection->prepare($sql)->execute($row);

        return $this->getEntry((int)$this->connection->lastInsertId())[0];
    }

    public function findExistingEntry($name) {

        $_name = [
            'name' => $name
        ];

        $sql = "SELECT * from streets where name=:name;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_name);

        return $prep->fetchAll();

    }


    public function getEntry($id) {

        $_id = [
            'id' => $id
        ];

        $sql = "SELECT * from streets where street_id=:id;";

        $prep = $this->connection->prepare($sql);

        $prep->execute($_id);

        return $prep->fetchAll();

    }
}

?>