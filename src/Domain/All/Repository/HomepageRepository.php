<?php

namespace App\Domain\All\Repository;
use PDO;

/**
 * Repository.
 */
class HomepageRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function setHomepageView(string $language, string $user_agent) {

        $_data = [
            'language' => $language,
            'user_agent' => $user_agent
        ];

        $sql = "INSERT INTO homepage_views
                SET 
                language=:language,
                user_agent=:user_agent;";

        $this->connection->prepare($sql)->execute($_data);

        return (int)$this->connection->lastInsertId();

    }
}
?>