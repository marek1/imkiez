<?php

namespace App\Domain\All\Repository;

use PDO;

/**
 * Repository.
 */
class CategoriesRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }


    public function retrieveAll()
    {

        $sql = "SELECT * from categories";

        $prep = $this->connection->prepare($sql);

        $prep->execute();

        return $prep->fetchAll();

    }
}

?>