<?php

namespace App\Domain\All\Data;

final class ImageAuthorizationData
{
    public $places_images_id;

    public $comment;

    public $user_id;

    public $approved;

}
?>