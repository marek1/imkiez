<?php

namespace App\Domain\All\Data;

final class VideoAuthorizationData
{
    public $places_videos_id;

    public $comment;

    public $user_id;

    public $approved;

}
?>