<?php

namespace App\Domain\All\Data;

final class PlaceAuthorizationData
{
    public $place_id;

    public $comment;

    public $user_id;

    public $approved;

}
?>