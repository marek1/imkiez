<?php

namespace App\Domain\All\Data;

final class PlacesVideosData
{
    public $place_id;

    public $video_url;

    public $user_id;

}