<?php

namespace App\Domain\All\Data;

final class RegisterData
{
    public $username;

    public $email;

    public $password;

}