<?php

namespace App\Domain\All\Data;

final class UserPostData
{
    public $username;

    public $description;

    public $img_url;

    public $thumb_url;

}