<?php

namespace App\Domain\All\Data;

final class PlaceUpdateData
{
    public $place_id;

    public $description;

    public $house_number;

    public $tags;

    public $ext_url;

    public $category_id;

}