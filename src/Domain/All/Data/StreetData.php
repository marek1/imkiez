<?php

namespace App\Domain\All\Data;

final class StreetData
{
    public $name;

    public $lat;

    public $lng;

    public $city;

    public $zip;

    public $district;

    public $county;

    public $state;

    public $countryName;
}