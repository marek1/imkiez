<?php

namespace App\Domain\All\Data;

final class PlacesWaypointsData
{
    public $place_id;

    public $waypoints;

    public $user_id;

}