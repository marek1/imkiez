<?php

namespace App\Domain\All\Data;

final class PlaceData
{
    public $name;

    public $description;

    public $street_id;

    public $house_number;

    public $lat;

    public $lng;

    public $img_url;

    public $thumb_url;

    public $video_url;

    public $category_id;

    public $tags;

    public $ext_url;

    public $int_url;
}