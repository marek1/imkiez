<?php

namespace App\Domain\All\Data;

final class PlacesImagesData
{
    public $place_id;

    public $img_url;

    public $thumb_url;

    public $user_id;

}