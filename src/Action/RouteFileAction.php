<?php

namespace App\Action;
use PhpParser\Node\Expr\Array_;
use Slim\Http\Response;
use Slim\Http\ServerRequest;
use function DI\string;

final class RouteFileAction
{
    public function __construct()
    {

    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        $uploaddir = __DIR__ . '/../../public/routes/';
        $uploadfile = $uploaddir . basename($_FILES['routeFile']['name']);

        if (move_uploaded_file($_FILES['routeFile']['tmp_name'], $uploadfile)) {
            $returnArray = array();
            $gpx = simplexml_load_file($uploadfile);
            foreach ($gpx->trk as $trk) {
                foreach($trk->trkseg as $seg){
                    foreach($seg->trkpt as $pt) {
                        //$_d = array($pt["lat"], $pt["lon"]);
                        // print_r($_d[0][0]);
                        // print_r($pt["lat"]->__toString());
                        array_push($returnArray, array($pt["lat"]->__toString(), $pt["lon"]->__toString()));
                    }
                }
            }
            return $response->withJson($returnArray)->withStatus(200);
        } else {
            return $response->withStatus(400);
        }



    }
}