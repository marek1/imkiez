<?php

namespace App\Action;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class UserProfileGetAction
{
    private $users;

    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        $id = $request->getParam('id');

        if (!$id || strlen($id) < 1) {
            return $response->withStatus(400);
        }

        $result = $this->users->getUserprofile($id);

        if (!$result) {
            return $response->withStatus(400);
        }

        return $response->withJson($result)->withStatus(201);

    }

}
?>