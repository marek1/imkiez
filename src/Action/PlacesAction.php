<?php

namespace App\Action;
use App\Domain\All\Service\Places;
use App\Domain\All\Service\Token;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class PlacesAction
{
    private $places;
    private $token;

    public function __construct(Places $places, Token $token)
    {
        $this->places = $places;
        $this->token = $token;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        //$result = $this->places->retrieveAll();
        $data = $request->getParsedBody();

        $token = $this->token->getToken(getallheaders());

        $user_id = null;
        if ($token && isset($token['user_id'])) {
            $user_id = $token['user_id'];
        }

        $results = $this->places->retrieveBySearchterm($data['searchTerm'], $data['lat'], $data['lng'], $user_id);

        return $response->withJson([
            'searchTerm' => $data['searchTerm'],
            'results' => $results
        ])->withStatus(201);

    }
}
?>