<?php

namespace App\Action;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class ThumbAction
{


    public function __construct()
    {

    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        $data = ['1585839617.jpg',
            '1585839680.jpg',
            '1585839763.jpg',
            '1585839866.jpg',
            '1585839970.jpg',
            '1585840044.jpg',
            '1585840151.jpg',
            '1585840281.jpg',
            '1585840403.jpg',
            '1585840461.jpg',
            '1585840532.jpg',
            '1585840618.jpg',
            '1585840728.jpg',
            '1585840799.jpg',
            '1585840855.jpg',
            '1585840906.jpg',
            '1585840972.jpg',
            '1585841055.jpg',
            '1585841123.jpg',
            '1585841250.jpg',
            '1585841290.jpg',
            '1585841344.jpg',
            '1585841386.jpg',
            '1585841454.jpg',
            '1585841494.jpg',
            '1585841621.jpg',
            '1585841700.jpg',
            '1585841893.jpg',
            '1585841989.jpg',
            '1585842148.jpg',
            '1585842315.jpg',
            '1585842391.jpg',
            '1585842485.jpg',
            '1585842610.jpg',
            '1585842645.jpg',
            '1585842713.jpg',
            '1585842757.jpg',
            '1585842792.jpg'
        ];

        $uploaddir = __DIR__ . '/../../public/uploads/';
        $thumbdir = $uploaddir .'/../thumbs/';

        foreach ($data as $img) {

            $uploadFileLocation = $uploaddir . $img;
            $this->create_thumb($uploadFileLocation, $thumbdir . $img);

        }

        return $response->withStatus(200);

    }

    private function create_thumb($url, $filename, $width = 640, $height = true)
    {

        // download and create gd image
        $image = ImageCreateFromString(file_get_contents($url));

        // calculate resized ratio
        // Note: if $height is set to TRUE then we automatically calculate the height based on the ratio
        $height = $height === true ? (ImageSY($image) * $width / ImageSX($image)) : $height;

        // create image
        $output = ImageCreateTrueColor($width, $height);
        ImageCopyResampled($output, $image, 0, 0, 0, 0, $width, $height, ImageSX($image), ImageSY($image));

        // save image
        ImageJPEG($output, $filename, 95);

        // return resized image
        return $output; // if you need to use it
    }

}
?>