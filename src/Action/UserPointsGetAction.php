<?php

namespace App\Action;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class UserPointsGetAction
{
    private $users;

    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $username = $request->getParam('id');

        if (!$username || strlen($username) < 1) {
            return $response->withStatus(400);
        }

        $result = $this->users->getPoints($username);

        if (!$result) {
            return $response->withStatus(400);
        }

        return $response->withJson($result)->withStatus(201);

    }
}