<?php

namespace App\Action;
use App\Domain\All\Service\Place;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class PlaceGetAction
{
    private $place;
    private $users;

    public function __construct(Place $place, Users $users)
    {
        $this->place = $place;
        $this->users = $users;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $int_url = $request->getParam('id');

        if (!$int_url || strlen($int_url) < 1) {
            return $response->withStatus(400);
        }

        $result = $this->place->retrieveByIntUrl($int_url);

        if (!$result) {
            return $response->withStatus(400);
        }

        return $response->withJson($result)->withStatus(201);
    }
}