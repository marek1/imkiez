<?php

namespace App\Action;
use App\Domain\All\Data\PlaceUpdateData;
use App\Domain\All\Service\Place;
use App\Domain\All\Service\Token;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class PlaceUpdateAction
{
    private $place;
    private $users;
    private $token;

    public function __construct(Place $place, Users $users, Token $token)
    {
        $this->place = $place;
        $this->users = $users;
        $this->token = $token;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $data = $request->getParsedBody();

        // TOKEN - START
        $token = $this->token->getToken(getallheaders());
        if (!$token) {
            return $response->withStatus(401);
        }
        if(!$this->token->checkTokenExpiry($token)) {
            return $response->withStatus(401);
        }
        // TOKEN - END

        $place_in_data = $data['place'];

        if (!$place_in_data['place_id']) {
            return $response->withStatus(400);
        }
        $place = new PlaceUpdateData();
        $place->place_id = $place_in_data['place_id'];
        $place->house_number = $place_in_data['house_number'];
        $place->description = $place_in_data['description'];
        $place->tags = $place_in_data['tags'];
        $place->ext_url = $place_in_data['ext_url'];
        $place->category_id = $place_in_data['category_id'];

        $placeId = $this->place->update($place);

        if (!$placeId) {
            return $response->withStatus(400);
        }

        $result = $this->place->retrieveById($place_in_data['place_id']);

        return $response->withJson($result)->withStatus(201);

    }
}
?>