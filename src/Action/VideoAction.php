<?php

namespace App\Action;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class VideoAction
{
    public function __construct()
    {

    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        $data = $request->getParsedBody();

        $video_file = $data["videoUrl"];

        if (strlen($video_file) < 1) {
            return $response->withStatus(401);
        }

        $video_file = preg_replace('#^data:video/[^;]+;base64,#', '', $video_file);

        $filename = time() . '.mp4';

        $tempdir = __DIR__ . '/../../public/videos/';

        // move_uploaded_file($_FILES["file"]["tmp_name"], $targetfolder . $_FILES["file"]["name"]);

        if (file_put_contents($tempdir . $filename, base64_decode($video_file))) {
            $result = [
                'video_url' => 'public/videos/' . $filename
            ];
            return $response->withJson($result)->withStatus(201);
        } else {
            return $response->withStatus(400);
        }

    }

}