<?php

namespace App\Action;
use App\Domain\All\Data\ImageAuthorizationData;
use App\Domain\All\Data\VideoAuthorizationData;
use App\Domain\All\Service\Email;
use App\Domain\All\Service\Place;
use App\Domain\All\Service\Token;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;
use function DI\string;

final class AssetAuthorizationAction
{
    private $place;
    private $token;
    private $users;
    private $email;

    public function __construct(Place $place, Users $users, Token $token, Email $email)
    {
        $this->place = $place;
        $this->token = $token;
        $this->users = $users;
        $this->email = $email;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        $data = $request->getParsedBody();

        if ((!isset($data['places_images_id']) || !isset($data['places_videos_id']))
            && !isset($data['approved'])) {
            return $response->withStatus(400);
        }

        // TOKEN - START
        $token = $this->token->getToken(getallheaders());
        if (!$token) {
            return $response->withStatus(401);
        }
        if(!$this->token->checkTokenExpiry($token)) {
            return $response->withStatus(401);
        }
        // TOKEN - END

        if (isset($data['places_images_id'])) {

            $authData = new ImageAuthorizationData();

            $authData->places_images_id = $data['places_images_id'];

            $authData->comment = $data['comment'];

            $authData->user_id = $token['user_id'];

            $authData->approved = $data['approved'];

            // check if image is not approved yet
            $approved = $this->place->check_if_image_approved($authData->places_images_id);

            if (!isset($approved['approved']) || $approved['approved'] == 1) {
                return $response->withStatus(400);
            }

            // creator cannot be approver
            $creatorId = $this->place->get_images_creator($authData->places_images_id);

            if (!isset($creatorId['user_id']) || $creatorId['user_id'] == $authData->user_id) {
                return $response->withStatus(400);
            }

            // check number of existing entries for this place_id
            // ^^^ NOPE we take everything we can get !!!
            // therefore save auth in any case :
            $saved = $this->place->save_images_authorization($authData);

            if (!$saved) {
                return $response->withStatus(400);
            }


            // TODO: perhaps implement, that at least 2 more approvals then denials need to exist
            // At the moment, only 1 approval ever is needed
            if ($data['approved'] == 1) {
                $set_approved = $this->place->set_image_approved($authData->places_images_id);
            }
        }

        if (isset($data['places_videos_id'])) {

            $authData = new VideoAuthorizationData();

            $authData->places_videos_id = $data['places_videos_id'];

            $authData->comment = $data['comment'];

            $authData->user_id = $token['user_id'];

            $authData->approved = $data['approved'];

            // check if image is not approved yet
            $approved = $this->place->check_if_video_approved($authData->places_videos_id);

            if (!isset($approved['approved']) || $approved['approved'] == 1) {
                return $response->withStatus(400);
            }

            // creator cannot be approver
            $creatorId = $this->place->get_videos_creator($authData->places_videos_id);

            if (!isset($creatorId['user_id']) || $creatorId['user_id'] == $authData->user_id) {
                return $response->withStatus(400);
            }

            // check number of existing entries for this place_id
            // ^^^ NOPE we take everything we can get !!!
            // therefore save auth in any case :
            $saved = $this->place->save_videos_authorization($authData);

            if (!$saved) {
                return $response->withStatus(400);
            }

            // TODO: perhaps implement, that at least 2 more approvals then denials need to exist
            // At the moment, only 1 approval ever is needed
            if ($data['approved'] == 1) {
                $set_approved = $this->place->set_video_approved($authData->places_videos_id);
            }
        }


        // Points for approver
        // Currently:
        // 0.5 points for approving or denying
        // + 0.5 points for leaving a comment > 100 chars

        $points = 0.25;
        if (strlen($authData->comment) > 100) {
            $points = $points + 0.25;
        }

        $this->users->save_points($authData->user_id, $points);

        // APPROVED:
        if ($data['approved'] == 1) {
            // GIVE POINTS :
            // Points for creator
            $points_for_creator = 1;
            $this->users->save_points($creatorId['user_id'], $points_for_creator);
        }

        // SEND EMAIL :

        // GET User E-Mail
        $user = $this->users->getUserEmail($creatorId['user_id']);
        if ($user['email']) {
            if (isset($data['places_videos_id'])) {
                if ($data['approved'] == 1) {
                    $headline = 'Dein Video wurde freigegeben';
                    $nachricht = '
                    <h4>
                        Wir freuen uns sehr dir mitteilen zu dürfen, daß dein Video freigegeben wurde. Du hast ' . $points_for_creator . ' Punkt(e) dafür bekommen.
                    </h4>
                ';
                } else {
                    $headline = 'Feedback zu deinem Video ist eingetroffen';
                    $nachricht = '
                        <h4>Jemand hat dein Video abgelehnt.</h4>
                        <h5>Aber das Video kann trotzdem noch verifiziert werden, wenn andere NutzerInnen es verifizieren.</h5>
                    ';
                }
            } else {
                if ($data['approved'] == 1) {
                    $headline = 'Dein Bild wurde freigegeben';
                    $nachricht = '
                    <h4>
                        Wir freuen uns sehr dir mitteilen zu dürfen, daß dein Bild freigegeben wurde. Du hast ' . $points_for_creator . ' Punkte dafür bekommen.
                    </h4>
                ';
                } else {
                    $headline = 'Feedback zu deinem Bild ist eingetroffen';
                    $nachricht = '
                        <h4>Jemand hat dein Bild abgelehnt.</h4>
                        <h5>Aber das Bild kann trotzdem noch verifiziert werden, wenn andere NutzerInnen es verifizieren.</h5>
                    ';
                }
            }
            if ($data['comment'] && strlen($data['comment']) > 0) {
                $nachricht .= '<p>Die Person hat noch Kommentar hinterlassen:</p>';
                $nachricht .= '<p><i>' . $data['comment'] . '</i></p>';
            }
            $nachricht .= '
                            <p>
                                Danke und weiter so, Das ImKiez-Team.
                            </p>
            ';
            $nachricht .= '
                <h6>
                    Der Link zum Ort: <a href="https://imkiez.de/imkiez/' . $data['asset_url'] . '" target="_blank">https://imkiez.de/imkiez/' . $data['asset_url'] . '</a>
                </h6>
            ';

            $this->email->send($user['email'], $headline, $nachricht);

        }

        return $response->withStatus(201);

    }
}
?>