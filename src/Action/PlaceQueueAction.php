<?php

namespace App\Action;
use App\Domain\All\Service\Place;
use App\Domain\All\Service\Token;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class PlaceQueueAction
{

    private $place;
    private $token;

    public function __construct(Place $place, Token $token)
    {
        $this->place = $place;
        $this->token = $token;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        // TOKEN - START
        $token = $this->token->getToken(getallheaders());
        if (!$token) {
            return $response->withStatus(401);
        }
        if(!$this->token->checkTokenExpiry($token)) {
            return $response->withStatus(401);
        }
        // TOKEN - END

        $result = $this->place->retrieve_non_approved($token['user_id']);

        return $response->withJson($result)->withStatus(201);

    }

}