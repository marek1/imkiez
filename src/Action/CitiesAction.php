<?php

namespace App\Action;
use App\Domain\All\Service\Cities;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class CitiesAction
{
    private $cities;

    public function __construct(Cities $cities)
    {
        $this->cities = $cities;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $result = $this->cities->retrieveAll();
        return $response->withJson($result)->withStatus(201);
    }
}
?>