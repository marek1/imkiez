<?php

namespace App\Action;
use App\Domain\All\Service\Pages;
use App\Domain\All\Service\Place;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class PageAction
{
    private $pages;
    private $place;

    public function __construct(Pages $pages, Place $place)
    {
        $this->pages = $pages;
        $this->place = $place;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $int_url = $request->getParam('id');

        if (!$int_url || strlen($int_url) < 1) {
            return $response->withStatus(400);
        }

        $result = $this->pages->retrieveByIntUrl($int_url);

        if (!$result) {
            return $response->withStatus(400);
        }

        // echo " 2 :  " . $result['places'];
        // need to split
        $places = explode(",", $result['places']);

        $result['places'] = array();
        foreach ($places as $place) {
            $_place = $this->place->retrieveById($place);
            array_push($result['places'], $_place);
        }

        return $response->withJson($result)->withStatus(201);

    }

}