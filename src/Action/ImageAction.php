<?php

namespace App\Action;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class ImageAction
{

    public function __construct()
    {

    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $data = $request->getParsedBody();

        $image_file = $data["imageUrl"];

        if (strlen($image_file) < 1) {
            return $response->withStatus(401);
        }

        $tempdir = __DIR__ . '/../../public/temp/';
        $uploaddir = $tempdir .'/../uploads/';
        $thumbdir = $tempdir .'/../thumbs/';

        $filename = time() . '.jpg';

        $uploadfile = $this->base64_to_jpeg($image_file, $filename);

        $uploadFileLocation = $tempdir . $uploadfile;

        if (rename($uploadfile, $uploadFileLocation)) {

            $this->create_image_copy($uploadFileLocation, $thumbdir . $uploadfile);

            $this->create_image_copy($uploadFileLocation, $uploaddir . $uploadfile, 1365);

            // Transform the result into the JSON representation
            $result = [
                'img_url' => 'public/uploads/' . $filename,
                'thumb_url' => 'public/thumbs/' . $filename
            ];
            // Build the HTTP response
            return $response->withJson($result)->withStatus(201);

        } else {
            // echo "Upload failed";
            return $response->withStatus(400);
        }

    }

    private function create_image_copy($url, $filename, $width = 640, $height = true)
    {

        // download and create gd image
        $image = ImageCreateFromString(file_get_contents($url));

        // calculate resized ratio
        // Note: if $height is set to TRUE then we automatically calculate the height based on the ratio
        $height = $height === true ? (ImageSY($image) * $width / ImageSX($image)) : $height;

        // create image
        $output = ImageCreateTrueColor($width, $height);
        ImageCopyResampled($output, $image, 0, 0, 0, 0, $width, $height, ImageSX($image), ImageSY($image));

        // save image
        ImageJPEG($output, $filename, 95);

        // return resized image
        return $output; // if you need to use it
    }


    private function base64_to_jpeg($base64_string, $output_file)
    {
        // open the output file for writing
        $ifp = fopen($output_file, 'wb');

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode(',', $base64_string);

        // we could add validation here with ensuring count( $data ) > 1
        fwrite($ifp, base64_decode($data[1]));

        // clean up the file resource
        fclose($ifp);

        return $output_file;
    }
}
?>