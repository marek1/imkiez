<?php

namespace App\Action;
use App\Domain\All\Data\PlaceAuthorizationData;
use App\Domain\All\Service\Email;
use App\Domain\All\Service\Place;
use App\Domain\All\Service\Token;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

final class PlaceAuthorizationAction
{
    private $place;
    private $users;
    private $email;
    private $token;

    public function __construct(Place $place, Users $users, Email $email, Token $token)
    {
        $this->place = $place;
        $this->users = $users;
        $this->email = $email;
        $this->token = $token;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        $data = $request->getParsedBody();

        if (!isset($data['place_id']) && !isset($data['approved'])) {
            return $response->withStatus(400);
        }

        // TOKEN - START
        $token = $this->token->getToken(getallheaders());
        if (!$token) {
            return $response->withStatus(401);
        }
        if(!$this->token->checkTokenExpiry($token)) {
            return $response->withStatus(401);
        }
        // TOKEN - END

        $authData = new PlaceAuthorizationData();

        $authData->place_id = $data['place_id'];

        $authData->comment = $data['comment'];

        $authData->user_id = $token['user_id'];

        $authData->approved = $data['approved'];

        // check if place is not approved yet
        $approved = $this->place->check_if_approved($authData->place_id);

        if (!isset($approved['approved']) || $approved['approved'] == 1) {
            return $response->withStatus(400);
        }

        // creator cannot be approver
        $creatorId = $this->place->get_place_creator($authData->place_id);

        if (!isset($creatorId['user_id']) || $creatorId['user_id'] == $authData->user_id) {
            return $response->withStatus(400);
        }

        // check number of existing entries for this place_id
        // ^^^ NOPE we take everything we can get !!!
        // therefore save auth in any case :
        $saved = $this->place->save_authorization($authData);

        if (!$saved) {
            return $response->withStatus(400);
        }

        // Points for creator
        $points_for_creator = 2;

        // Points for approver
        // Currently:
        // 0.5 points for approving or denying
        // + 0.5 points for leaving a comment > 100 chars

        $points = 0.5;
        if (strlen($authData->comment) > 100) {
            $points = $points + 0.5;
        }

        $this->users->save_points($authData->user_id, $points);

        // and update place if

        $numberOfApprovedMinusDeclined_1 = $this->place->get_place_auths_diff($authData);

        if ($numberOfApprovedMinusDeclined_1 == $points_for_creator) {
            $set_approved = $this->place->set_approved($authData->place_id);
            if($set_approved) {
                // give creator the points
                if (isset($creatorId['user_id'])) {
                    // GIVE POINTS !!!
                    $this->users->save_points($creatorId['user_id'], $points_for_creator);
                    // ^^^^^^^^^^^^^^^
                    // GET PLACE Details
                    $place = $this->place->retrieveByIdSlim($authData->place_id);
                    // GET User E-Mail
                    $user = $this->users->getUserEmail($creatorId['user_id']);
                    if ($place['name'] && $user['email']) {
                        $headline = 'Dein Ort wurde freigegeben';
                        $nachricht = '
                            <h4>
                                Wir freuen uns sehr dir mitteilen zu dürfen, daß dein Ort freigegeben wurde. Du hast '.$points_for_creator.' Punkte dafür bekommen.
                            </h4>
                        ';
                        if ($data['comment'] && strlen($data['comment']) > 0) {
                            $nachricht .= '<p>Die Person hat einen Kommentar hinterlassen:</p>';
                            $nachricht .= '<p><i>' . $data['comment'] . '</i></p>';
                        }
                        $nachricht .= '
                            <p>
                                Danke und weiter so, Das ImKiez-Team.
                            </p>
                            <h6>
                                Der Link zum Ort: <a href="https://imkiez.de/ort/'. $place['int_url'] .'" target="_blank">'. $place['name'] .'</a>
                            </h6>
                        ';

                        $this->email->send($user['email'], $headline, $nachricht);

                    }

                }
            }
        } else {
            // notify creator, that the place has received something
            if (isset($creatorId['user_id'])) {
                // GET PLACE Details
                $place = $this->place->retrieveByIdSlim($authData->place_id);
                // GET User E-Mail
                $user = $this->users->getUserEmail($creatorId['user_id']);
                if ($place['name'] && $user['email']) {
                    $headline = 'Feedback zu deinem Ort ist eingetroffen';

                    if ($data['approved'] == 1) {
                        $nachricht = '
                            <h4>Super Nachrichten. Jemand hat deinen Ort verifiziert.</h4>';
                    } else {
                        $nachricht = '
                            <h4>Jemand hat deinen Eintrag abgelehnt.</h4>
                            <h5>Aber der Ort kann trotzdem verifiert werden, wenn andere Leute ihn verifizieren.</h5>';
                    }
                    if ($data['comment'] && strlen($data['comment']) > 0) {
                        $nachricht .= '<p>Die Person hat einen Kommentar hinterlassen:</p>';
                        $nachricht .= '<p><i>' . $data['comment'] . '</i></p>';
                    }
                    $nachricht .= '<p>Danke & Grüße, Das ImKiez-Team</p>';
                    $nachricht .= '
                        <h6>
                        Der Link zum Ort: <a href="https://imkiez.de/ort/'. $place['int_url'] .'" target="_blank">'. $place['name'] .'</a>
                        </h6>
                    ';

                    $this->email->send($user['email'], $headline, $nachricht);

                }

            }
        }

        return $response->withStatus(201);


    }
}
?>