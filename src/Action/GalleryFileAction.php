<?php

namespace App\Action;
use PhpParser\Node\Expr\Array_;
use Slim\Http\Response;
use Slim\Http\ServerRequest;
use function DI\string;

final class GalleryFileAction
{
    public function __construct()
    {

    }

    private function create_image_copy($url, $filename, $width = 640, $height = true)
    {

        // download and create gd image
        $image = ImageCreateFromString(file_get_contents($url));

        // calculate resized ratio
        // Note: if $height is set to TRUE then we automatically calculate the height based on the ratio
        $height = $height === true ? (ImageSY($image) * $width / ImageSX($image)) : $height;

        // create image
        $output = ImageCreateTrueColor($width, $height);
        ImageCopyResampled($output, $image, 0, 0, 0, 0, $width, $height, ImageSX($image), ImageSY($image));

        // save image
        ImageJPEG($output, $filename, 95);

        // return resized image
        return $output; // if you need to use it
    }

    public function convertImage($originalImage, $outputImage, $quality)
    {
        // jpg, png, gif or bmp?
        $exploded = explode('.',$originalImage);
        $ext = $exploded[count($exploded) - 1];

        if (preg_match('/jpg|jpeg/i',$ext))
            $imageTmp=imagecreatefromjpeg($originalImage);
        else if (preg_match('/png/i',$ext))
            $imageTmp=imagecreatefrompng($originalImage);
        else if (preg_match('/gif/i',$ext))
            $imageTmp=imagecreatefromgif($originalImage);
        else
            return 0;

        // quality is a value from 0 (worst) to 100 (best)
        imagejpeg($imageTmp, $outputImage, $quality);
        imagedestroy($imageTmp);

        return 1;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        $tempdir = __DIR__ . '/../../public/temp/';

        // The uploaded file type [image/jpeg]
        $type = $_FILES['galleryFile']["type"];

        // image format declaring in an array
        $img_type = array('image/jpeg',
            'image/jpg',
            'image/png',
            'image/gif');

        // video format declaring in an array
        $video_type = array('video/mp4',
            'video/m4v',
            'video/mov',
            'video/ogg',
            'video/3gp',
            'video/wmv',
            'video/webm',
            'video/flv',
            'video/avi',
            'video/quicktime');



        if (in_array($type, $img_type)) {

            /*
             * IMAGE!!!
             */

            $uploaddir = $tempdir .'/../uploads/';
            $thumbdir = $tempdir .'/../thumbs/';

            $tempfile = $tempdir . basename($_FILES['galleryFile']['name']);

            $filename = substr(md5(basename($_FILES['galleryFile']['name'])), 0,10) . '-' . time() . '.jpg';

            $fileLocation = $tempdir . $filename;

            if (move_uploaded_file($_FILES['galleryFile']['tmp_name'], $tempfile)) {

                // Step1 : Create jpg
                $this->convertImage($tempfile, $fileLocation, 100);

                // Step2: Copy to uploads & thumbs

                $this->create_image_copy($fileLocation, $thumbdir . $filename);

                $this->create_image_copy($fileLocation, $uploaddir . $filename, 1365);

                // Transform the result into the JSON representation
                $result = [
                    'img_url' => 'public/uploads/' . $filename,
                    'thumb_url' => 'public/thumbs/' . $filename
                ];

                return $response->withJson($result)->withStatus(200);
            } else {
                return $response->withStatus(400);
            }
        } elseif (in_array($type, $video_type)) {

            /*
             *  VIDEO !!!
             */

            $uploaddir = $tempdir .'/../videos/';

            $uploadfile = basename($_FILES['galleryFile']['name']);

            $uploaddir = $uploaddir . $uploadfile;

            if (move_uploaded_file($_FILES['galleryFile']['tmp_name'], $uploaddir)) {
                $result = [
                    'video_url' => 'public/videos/' . $uploadfile
                ];
                return $response->withJson($result)->withStatus(201);
            } else {
                /*
                $result = [
                    'text' => 'video could not be saved'
                ];
                return $response->withJson($result)->withStatus(201);
                */

                return $response->withStatus(400);
            }

        } else {
            /*
            $result = [
                'text' => 'neither video nor image ; type '. $type
            ];
            return $response->withJson($result)->withStatus(201);
            */
            return $response->withStatus(400);
        }


    }
}