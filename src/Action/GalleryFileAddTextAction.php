<?php

namespace App\Action;
use App\Domain\All\Service\Place;
use Slim\Http\Response;
use Slim\Http\ServerRequest;
use function DI\string;

final class GalleryFileAddTextAction
{
    private $place;
    private $token;

    public function __construct(Place $place, Token $token)
    {
        $this->place = $place;
        $this->token = $token;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $data = $request->getParsedBody();

        // TOKEN - START
        $token = $this->token->getToken(getallheaders());
        if (!$token) {
            return $response->withStatus(401);
        }
        if(!$this->token->checkTokenExpiry($token)) {
            return $response->withStatus(401);
        }
        // TOKEN - END

        $video_url = $data['video_url'];
        $img_url = $data['img_url'];
        $description = $data['description'];

        if ((!$video_url && !$img_url) || !$description) {
            return $response->withStatus(400);
        } elseif ($video_url) {
            $this->place->add_video_description($description, $video_url);
        } elseif ($img_url) {
            $this->place->add_image_description($description, $img_url);
        }

        return $response->withStatus(200);

    }


}