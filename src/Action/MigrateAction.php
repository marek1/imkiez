<?php

namespace App\Action;
use App\Domain\All\Service\Places;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class MigrateAction
{
    private $places;

    public function __construct(Places $places)
    {
        $this->places = $places;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        
        $this->places->migrateAssets();

        return $response->withStatus(200);
    }
}