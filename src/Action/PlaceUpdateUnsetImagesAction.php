<?php

namespace App\Action;
use App\Domain\All\Data\PlacesImagesData;
use App\Domain\All\Service\Place;
use App\Domain\All\Service\Token;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class PlaceUpdateUnsetImagesAction
{
    private $place;
    private $users;
    private $token;

    public function __construct(Place $place, Users $users, Token $token)
    {
        $this->place = $place;
        $this->users = $users;
        $this->token = $token;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $data = $request->getParsedBody();

        // TOKEN - START
        $token = $this->token->getToken(getallheaders());
        if (!$token) {
            return $response->withStatus(401);
        }
        if(!$this->token->checkTokenExpiry($token)) {
            return $response->withStatus(401);
        }
        // TOKEN - END

        $place_in_data = $data['place'];

        if (!$place_in_data['place_id']) {
            return $response->withStatus(400);
        }
        $place = new PlacesImagesData();
        $place->place_id = $place_in_data['place_id'];
        $place->img_url = $place_in_data['img_url'];
        $place->thumb_url = $place_in_data['thumb_url'];
        $place->user_id = $token['user_id'];

        $placeId = $this->place->unset_images($place);

        if (!$placeId) {
            return $response->withStatus(400);
        }

        $result = [
            'img_url' => $place_in_data['img_url'],
            'thumb_url' => $place_in_data['thumb_url']
        ];

        return $response->withJson($result)->withStatus(201);

    }
}
?>