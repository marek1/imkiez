<?php

namespace App\Action;
use App\Domain\All\Service\Categories;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class CategoriesAction
{
    private $categories;

    public function __construct(Categories $categories)
    {
        $this->categories = $categories;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $result = $this->categories->retrieveAll();
        return $response->withJson($result)->withStatus(201);
    }
}
?>