<?php

namespace App\Action;
use App\Domain\All\Data\UserPostData;
use App\Domain\All\Service\Token;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class UserProfilePostAction
{
    private $users;
    private $token;

    public function __construct(Users $users, Token $token)
    {
        $this->users = $users;
        $this->token = $token;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $data = $request->getParsedBody();

        // TOKEN - START
        $token = $this->token->getToken(getallheaders());
        if (!$token) {
            return $response->withStatus(401);
        }
        if(!$this->token->checkTokenExpiry($token)) {
            return $response->withStatus(401);
        }
        // TOKEN - END

        $user_data = $data['user'];

        // var_dump($place_in_data);

        if (!$user_data['username']) {
            return $response->withStatus(400);
        }

        $result = $this->users->checkUsername($user_data['username'], $token['token']);

        if ($result) {

            // do update
            $user = new UserPostData();
            $user->username = $user_data['username'];
            $user->description = $user_data['description'];
            $user->img_url = $user_data['img_url'];
            $user->thumb_url = $user_data['thumb_url'];

            $userId = $this->users->updateUserprofile($user);

            if (!$userId) {
                return $response->withStatus(400);
            }

            return $response->withStatus(201);

        } else {
            return $response->withStatus(400);
        }
    }
}
?>