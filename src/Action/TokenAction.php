<?php

namespace App\Action;
use App\Domain\All\Service\Token;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class TokenAction
{
    private $users;
    private $token;

    public function __construct(Users $users, Token $token)
    {
        $this->users = $users;
        $this->token = $token;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        // TOKEN - START
        $token = $this->token->getToken(getallheaders());
        if (!$token) {
            return $response->withStatus(401);
        }
        if(!$this->token->checkTokenExpiry($token)) {
            return $response->withStatus(401);
        }
        // TOKEN - END

        //echo "user_id " .$token['user_id']. " // ";
        $user = $this->users->getUsername($token['user_id']);

        //echo "$user " .$user['username']. " // ";
        return $response->withJson($user['username'])->withStatus(201);

    }

}
?>