<?php

namespace App\Action;
use Slim\Http\Response;
use Slim\Http\ServerRequest;
use App\Domain\All\Service\Email;

final class KiezboxAction
{
    private $email;

    public function __construct(Email $email)
    {
        $this->email = $email;
    }


    public function __invoke(ServerRequest $request, Response $response): Response
    {

        $data = $request->getParsedBody();

        if (!isset($data['email'])) {
            return $response->withStatus(400);
        }

        // collect
        echo "email : " . $data['email'];
        echo "full_name : " . $data['full_name'];
        echo "shipping : " . $data['shipping'];
        echo "address : " . $data['address'];
        echo "items : " . $data['items'];

        $headline = 'Deine Kiez-Box';
        $nachricht = '<h3>Hallo ' .$data['full_name'] . '</h3>';
        $nachricht.= '<p>Vielen Dank für deinen Kauf einer Kiezbox und deiner Unterstützung.</p>';
        $nachricht.= '<p>Deine Kiezbox enthält folgende Produkte:</p>';
        $nachricht.= '<ul>';
        foreach (explode(", ", $data['items']) as $item) {
            $nachricht.= '<li>' . $item . '</li>';
        }
        $nachricht.= '</ul>';
        if ($data['shipping'] !== 'Abholung') {
            $nachricht.= '<p>Die Kiezbox wird an folgende Adresse gesendet:</p>';
            $nachricht.= '<p>'.$data['address'].'</p>';
            $nachricht.= '<p>Eine Versandbestätigung folgt.</p>';
        } else {
            $nachricht.= '<p>Die Kiezbox ist in wenigen Tagen abholbereit. Genaue Instruktionen folgen.</p>';
        }
        $nachricht .= '
                <p>
                    Danke & Grüße, Das ImKiez-Team.
                </p>
        ';
        $this->email->send($data['email'], $headline, $nachricht);

        return $response->withStatus(201);

    }

}