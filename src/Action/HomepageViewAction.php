<?php

namespace App\Action;
use App\Domain\All\Service\Homepage;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class HomepageViewAction
{

    private $homepage;

    public function __construct(Homepage $homepage)
    {
        $this->homepage = $homepage;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        $headers = getallheaders();

        $this->homepage->setHomepageView($headers['Accept-Language'], $headers['User-Agent']);

        return $response->withStatus(200);
    }

}

?>