<?php

namespace App\Action;
use App\Domain\All\Data\RegisterData;
use App\Domain\All\Service\Users;
use App\Domain\All\Service\Email;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class RegisterAction
{
    private $users;
    private $email;

    public function __construct(Users $users, Email $email)
    {
        $this->users = $users;
        $this->email = $email;
    }

    private function rand_string( $length ) {

        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars),0,$length);

    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        $register_data = $request->getParsedBody();

        if (!isset($register_data['username']) && !isset($register_data['email'])) {
            return $response->withStatus(400);
        }

        if (!ctype_alnum($register_data['username']) || strlen($register_data['username']) < 3)
        {
            return $response->withStatus(400);
        }

        $pass = $this->rand_string(8);

        // echo "$pass : ".$pass;

        $registerData = new RegisterData();
        $registerData->username = $register_data['username'];
        $registerData->email = $register_data['email'];
        $registerData->password = $pass;

        $check_if_user_exists = $this->users->checkIfUserExists($registerData);

        if ($check_if_user_exists) {

            return $response->withStatus(401);

        } else {

            $userId = $this->users->createUser($registerData);

            if (!$userId) {
                return $response->withStatus(400);
            }

            $result = [
                'user_id' => $userId
            ];

            $headline = 'Du bist jetzt Kiez-Scout';
            $nachricht = '
                <h4>Wir freuen uns sehr, dich in unserem Team begrüssen zu dürfen.</h4>
                <p>
                Dein Nutzername ist deine E-Mail und das Passwort lautet <b>' . $pass . '</b> 
                </p>
                <p>
                Dein Profil findest du hier : <a href="https://imkiez.de/user/' . $register_data['username'] . '" target="_blank">https://imkiez.de/user/' . $register_data['username'] . '</a>  
                </p>
                <p>
                Danke schon einmal fürs Mithelfen :)
                </p>
            ';

            $this->email->send($registerData->email, $headline, $nachricht);

            return $response->withJson($result)->withStatus(201);


        }

    }
}