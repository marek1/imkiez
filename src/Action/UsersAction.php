<?php

namespace App\Action;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class UsersAction
{
    private $users;

    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        $data = $request->getParsedBody();

        if (!isset($data['username']) || !isset($data['password'])) {
            return $response->withStatus(400);
        }

        $user = $this->users->getUser($data['username']);

        if (!$user) {

            return $response->withStatus(401);

        } else {

            if (password_verify($data['password'], $user['passwordhash'])) {
                // insert session !
                $sessionToken = $this->users->insertSession($user['user_id']);
                return $response->withJson([
                    'username' => $user['username'],
                    'sessionToken' => $sessionToken
                ])->withStatus(201);
            } else {
                return $response->withStatus(401);
            }
        }

    }

}
?>