<?php

namespace App\Action;
use App\Domain\All\Service\Place;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class PlaceAssetsAction
{
    private $place;
    private $users;

    public function __construct(Place $place, Users $users)
    {
        $this->place = $place;
        $this->users = $users;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $id = $request->getParam('id');

        if (!$id || strlen($id) < 1) {
            return $response->withStatus(400);
        }

        $result = $this->place->retrieve_assets($id);

        if (!$result) {
            return $response->withStatus(400);
        }

        return $response->withJson($result)->withStatus(201);
    }
}