<?php

namespace App\Action;
use App\Domain\All\Data\StreetData;
use App\Domain\All\Service\Streets;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class StreetsAction
{
    private $streets;

    public function __construct(Streets $streets)
    {
        $this->streets = $streets;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $data = $request->getParsedBody();

        $receivedStreets = $data['data']['items'];

        $returnedStreets = [];

        foreach ($receivedStreets as $street) {
            $newStreet = new StreetData();
            $newStreet->name = $street['address']['street'] ? $street['address']['street'] : $street['address']['label'];
            $newStreet->lat = $street['position']['lat'];
            $newStreet->lng = $street['position']['lng'];
            $newStreet->city = $street['address']['city'];
            $newStreet->zip = $street['address']['postalCode'];
            $newStreet->district = $street['address']['district'];
            $newStreet->county = $street['address']['county'];
            $newStreet->state = $street['address']['state'];
            $newStreet->countryName = $street['address']['countryName'];

            $returnedStreet = $this->streets->save($newStreet);

            array_push($returnedStreets, $returnedStreet);
        }

        $result = $returnedStreets;

        return $response->withJson($result)->withStatus(201);

    }
}
?>