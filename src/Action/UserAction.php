<?php

namespace App\Action;
use App\Domain\All\Service\Token;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class UserAction
{
    private $users;
    private $token;

    public function __construct(Users $users, Token $token)
    {
        $this->users = $users;
        $this->token = $token;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {

        // TOKEN - START
        $token = $this->token->getToken(getallheaders());
        if (!$token) {
            return $response->withStatus(401);
        }
        if(!$this->token->checkTokenExpiry($token)) {
            return $response->withStatus(401);
        }
        // TOKEN - END

        $username = $request->getParam('id');

        if (!$username || strlen($username) < 1) {
            return $response->withStatus(400);
        }

        $result = $this->users->checkUsername($username, $token['token']);

        if($result) {
            return $response->withStatus(200);
        } else {
            return $response->withStatus(400);
        }

    }

}
?>