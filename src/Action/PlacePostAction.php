<?php

namespace App\Action;
use App\Domain\All\Data\PlaceData;
use App\Domain\All\Service\Place;
use App\Domain\All\Service\Token;
use App\Domain\All\Service\Users;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class PlacePostAction
{
    private $place;
    private $users;
    private $token;

    public function __construct(Place $place, Users $users, Token $token)
    {
        $this->place = $place;
        $this->users = $users;
        $this->token = $token;
    }

    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $data = $request->getParsedBody();

        // TOKEN - START
        $token = $this->token->getToken(getallheaders());
        if (!$token) {
            return $response->withStatus(401);
        }
        if(!$this->token->checkTokenExpiry($token)) {
            return $response->withStatus(401);
        }
        // TOKEN - END

        $place_in_data = $data['place'];

        if (!$place_in_data['name']) {
            return $response->withStatus(400);
        }
        $place = new PlaceData();
        $place->name = $place_in_data['name'];
        $place->description = $place_in_data['description'];
        $place->street_id = $place_in_data['street_id'];
        $place->house_number = $place_in_data['house_number'];
        $place->lat = $place_in_data['lat'];
        $place->lng = $place_in_data['lng'];
        $place->img_url = $place_in_data['img_url'];
        $place->thumb_url = $place_in_data['thumb_url'];
        $place->video_url = $place_in_data['video_url'];
        $place->category_id = $place_in_data['category_id'];
        $place->tags = $place_in_data['tags'];
        $place->ext_url = $place_in_data['ext_url'];
        $place->int_url = $place_in_data['int_url'];
        $place->user_id = $token['user_id'];

        $placeId = $this->place->save($place);

        if (!$placeId) {
            return $response->withStatus(400);
        }

        $result = [
            'int_url' => $placeId
        ];

        return $response->withJson($result)->withStatus(201);

    }
}
?>