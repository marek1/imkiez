<?php

namespace App\Action;
use Slim\Http\Response;

final class HomeAction
{
    public function __construct(Response $response) {
        $result = ['error' => ['message' => 'Validation failed']];
		return $response->withJson($result)->withStatus(422);
    }
}